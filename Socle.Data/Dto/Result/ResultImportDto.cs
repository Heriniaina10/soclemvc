﻿using System.Collections.Generic;
using System.ComponentModel;
using Socle.Data.Enums;

namespace Socle.Data.Dto.Result
{
    public class ResultImportDto
    {
        [DefaultValue(OperationFlags.NOP)]
        public OperationFlags Operation { get; set; }
        public int TotalRow { get; set; }
        public int ValidRow { get; set; }
        public IList<ResultTemporaire> ResultDoublons { get; set; } = new List<ResultTemporaire>();
        public IList<ResultTemporaire> ResultErreurFormats { get; set; } = new List<ResultTemporaire>();
        public bool ImportWithError => ResultDoublons.Count > 0 || ResultErreurFormats.Count > 0;
    }

    public class ResultTemporaire
    {
        /// <summary>
        /// numero ligne dans le fichier .csv
        /// </summary>
        public int NumeroLigne { get; set; }
        /// <summary>
        /// Code Result dans le fichier .csv 
        /// </summary>
        public string CodeResult { get; set; }
        /// <summary>
        /// Result 
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// Message d'erreur.Non null si il y a erreur format. Pour les doublons, cette valeur est toujours null
        /// </summary>
        public string MessageErreur { get; set; }
        /// <summary>
        /// ligne
        /// </summary>
        public string Ligne { get; set; }

        public string Cadence { get; set; }
    }
}
