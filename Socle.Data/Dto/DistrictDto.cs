﻿using System;
using System.Collections.Generic;

namespace Socle.Data.Dto
{
    public class DistrictDto : ATerritoireDto
    {
        public Guid? CommuneId { get; set; }
        public virtual CommuneDto Commune { get; set; }

        public virtual ICollection<FokontanyDto> FokontanyList { get; set; }
    }
}
