﻿using System;
using System.Collections.Generic;
using Socle.Data.Dto.Personne;

namespace Socle.Data.Dto
{
    public class FonctionDto
    {
        public Guid Id { get; set; }
        public string Nom { get; set; }

        public virtual ICollection<PersonneDto> Personnes { get; set; }
    }
}
