﻿using System.Collections.Generic;
using Socle.Data.Dto.Personne;

namespace Socle.Data.Dto
{
    public class FokontanyDto : ATerritoireDto
    {
        public virtual DistrictDto District { get; set; }

        public virtual ICollection<PersonneDto> Personnes { get; set; }
    }
}
