﻿using System.Collections.Generic;

namespace Socle.Data.Dto
{
    public class ProvinceDto : ATerritoireDto
    {
        public ICollection<RegionDto> Regions { get; set; }
    }
}
