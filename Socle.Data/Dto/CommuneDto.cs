﻿using System;
using System.Collections.Generic;

namespace Socle.Data.Dto
{
    public class CommuneDto : ATerritoireDto
    {
        public Guid? RegionId { get; set; }
        
        public virtual RegionDto Region { get; set; }

        public virtual ICollection<DistrictDto> Districts { get; set; }
    }
}
