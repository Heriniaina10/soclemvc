﻿using System;
using System.Collections.Generic;

namespace Socle.Data.Dto
{
    public class RegionDto : ATerritoireDto
    {
        public Guid ProvinceId { get; set; }
        public virtual ProvinceDto Province { get; set; }

        public virtual ICollection<CommuneDto> Communes { get; set; }
    }
}
