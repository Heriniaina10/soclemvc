﻿using Socle.Data.Entities;

namespace Socle.Data.Dto.Personne
{
    public class PersonDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int? ContactId { get; set; }
        public Contact Contact { get; set; }
    }
}
