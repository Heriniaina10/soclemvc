﻿using System;
using System.Runtime.Serialization;
using Application.Etech.Core.Converters.Csv;
using FileHelpers;

namespace Socle.Data.Dto.Personne
{
    [DelimitedRecord(";")]
    public class PersonneCsvDto
    {
        //[FieldOrder(0)]
        //[FieldConverter(typeof(CustomNumberConverter), "Cadence", CustomNumberTypes.NullableDouble, ",")]
        //public double? Cadence;

        [FieldOrder(0)]
        public string Id { get; set; }
        [FieldOrder(1)]
        [FieldNotEmpty]
        public string NomComplet { get; set; }
        [FieldOrder(2)]
        public string DateDeNaissance { get; set; }
        [FieldOrder(3)]
        public string LieuDeNaissance { get; set; }
        [FieldOrder(4)]
        public string CIN { get; set; }
        [FieldOrder(5)]
        public string DateCIN { get; set; }
        [FieldOrder(6)]
        public string LieuCIN { get; set; }
        [FieldOrder(7)]
        public string DateDuplicata { get; set; }
        [FieldOrder(8)]
        public string LieuDuplicata { get; set; }
        [FieldOrder(9)]
        public string NumeroCarteBapteme { get; set; }
        [FieldOrder(10)]
        public string DateBapteme { get; set; }
        [FieldOrder(11)]
        public string LieuBapteme { get; set; }
        [FieldOrder(12)]
        public string Eglise { get; set; }
        [FieldOrder(13)]
        public string District { get; set; }
        [FieldOrder(14)]
        public string Region { get; set; }
        [FieldOrder(15)]
        public string Porte { get; set; }
        [FieldOrder(16)]
        public string Commune { get; set; }
        [FieldOrder(17)]
        public string Province { get; set; }

        //public Guid? FokontanyId { get; set; }
        //public Guid? FonctionId { get; set; }
    }
}
