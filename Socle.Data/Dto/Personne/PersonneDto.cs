﻿using System;

namespace Socle.Data.Dto.Personne
{
    public class PersonneDto
    {
        public int Id { get; set; }
        public bool Actif { get; set; }
        public int? Age { get; set; }
        public string CIN { get; set; }
        public DateTime DateBapteme { get; set; }
        public DateTime DateCIN { get; set; }
        public DateTime DateDeNaissance { get; set; }
        public DateTime DateDuplicata { get; set; }
        public bool IsDuplicata { get; set; }
        public string LieuBapteme { get; set; }
        public string LieuCIN { get; set; }
        public string LieuDeNaissance { get; set; }
        public string LieuDuplicata { get; set; }
        public string NomComplet { get; set; }
        public string NumeroCarteBapteme { get; set; }
        public string Section { get; set; }
        public int Numero { get; set; }

        public Guid? FokontanyId { get; set; }
        public virtual FokontanyDto Fokontany { get; set; }

        public Guid? FonctionId { get; set; }
        public virtual FonctionDto Fonction { get; set; }
    }
}
