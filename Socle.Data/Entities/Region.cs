using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Socle.Data.Entities
{
	/// <summary>
	/// Province : Province
	/// Communes : ICollection<Commune>
	/// </summary>
	public class Region : ATerritoire
    {
        public int ProvinceId { get; set; }
        [ForeignKey("ProvinceId")]
        public virtual Province Province { get; set; }

        public virtual ICollection<Commune> Communes { get; set; }
    }
}