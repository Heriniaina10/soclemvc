using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Socle.Data.Entities
{
	/// <summary>
	/// Region : Region
	/// Districts : ICollection<District>
	/// </summary>
	public class Commune : ATerritoire
    {
        public int? RegionId { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }

		public virtual ICollection<District> Districts { get; set; }
	}
}