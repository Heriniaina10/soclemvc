using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Socle.Data.Entities
{
	/// <summary>
	/// Commune : Commune
	/// Fokontanys : ICollection<Fokotany>
	/// </summary>
	public class District : ATerritoire
    {
        public int? CommuneId { get; set; }
        [ForeignKey("CommuneId")]
        public virtual Commune Commune { get; set; }

		public virtual ICollection<Fokontany> FokontanyList { get; set; }
	}
}