﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Socle.Data.Entities
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        //public ICollection<Person> Persons { get; set; }
    }
}
