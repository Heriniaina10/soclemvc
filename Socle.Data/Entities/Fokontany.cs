using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Socle.Data.Entities
{
    /// <summary>
    /// District : District
    /// Personnes : ICollection <Personnes>
    /// </summary>
    [Table("Fokontany")]
    public class Fokontany : ATerritoire
    {
        public int? DistrictId { get; set; }
        [ForeignKey("DistrictId")]
        public virtual District District { get; set; }

        public virtual ICollection<Personne> Personnes { get; set; }
    }
}