using System.Collections.Generic;

namespace Socle.Data.Entities
{
	public class Province : ATerritoire
    {
        public ICollection<Region> Regions { get; set; }
	}
}