using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Socle.Data.Entities
{
	public class Personne
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool Actif { get; set; } = true;
        public int? Age { get; set; }
        public string CIN { get; set; }
        public DateTime? DateBapteme { get; set; }
        public DateTime? DateCIN { get; set; }
        public DateTime? DateDeNaissance { get; set; }
        public DateTime? DateDuplicata { get; set; }
        public bool IsDuplicata { get; set; } = false;
        public string LieuBapteme { get; set; }
        public string LieuCIN { get; set; }
        public string LieuDeNaissance { get; set; }
        public string LieuDuplicata { get; set; }
        public string NomComplet { get; set; }
        public string NumeroCarteBapteme { get; set; }

        public string Section { get; set; }
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [NotMapped] public int Numero => Id;

        public int? FokontanyId { get; set; }
        [ForeignKey("FokontanyId")]
        public virtual Fokontany Fokontany { get; set; }

        public Guid? FonctionId { get; set; }
        [ForeignKey("FonctionId")]
		public virtual Fonction Fonction { get; set; }
	}
}