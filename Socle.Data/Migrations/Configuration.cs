using System;
using System.Collections.Generic;
using Socle.Data.Entities;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Socle.Data.Context;

namespace Socle.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Socle.Data.Context.SocleDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Socle.Data.Context.SocleDbContext context)
        {
            try
            {
                if (!context.Persons.Any())
                {
                    var persons = new List<Person>
                    {
                        new Person {FirstName = "rekaba", LastName = "jonadaba"},
                        new Person {FirstName = "Jefta", LastName = "jefta"},
                        new Person {FirstName = "hery ny aina", LastName = "eddy"},
                        new Person {FirstName = "paul", LastName = "saul"}
                    };

                    context.Persons.AddRange(persons);
                    context.SaveChanges();
                }

                if (!context.Contacts.Any())
                {
                    var contacts = new List<Contact>
                    {
                        new Contact { Address = "tana", PhoneNumber = "0000" },
                        new Contact { Address = "diego", PhoneNumber = "1111" },
                        new Contact { Address = "Majunga", PhoneNumber = "2222" },
                        new Contact { Address = "fianarantsoa", PhoneNumber = "3333" },
                    };

                    context.Contacts.AddRange(contacts);
                    context.SaveChanges();
                }

                if (context.Persons.Any() && context.Contacts.Any())
                {
                    UpdateDatas(context, "jonadaba", "tana");
                    UpdateDatas(context, "saul", "fianarantsoa");
                    UpdateDatas(context, "Jefta", "Majunga");
                    UpdateDatas(context, "eddy", "tana");
                    context.SaveChanges();
                }

                InitializeData(context);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }

        private void UpdateDatas(SocleDbContext context, string lastname, string address)
        {
            if (string.IsNullOrEmpty(lastname) || string.IsNullOrEmpty(address)) return;

            var person = context.Persons.FirstOrDefault(p => p.LastName.Equals(lastname));
            if (person == null) return;

            var contact = context.Contacts.FirstOrDefault(c => c.Address.Equals(address));
            person.Contact = contact;
        }

        private void AddProvinces(SocleDbContext ctx)
        {
            if (ctx.Provinces.Any())
            {
                return;
            }

            var provinces = new List<Province>
            {
                new Province { Nom = "Antananarivo", Code = "101"},
                new Province { Nom = "Toamasina", Code = "202"},
                new Province { Nom = "Mahajanga", Code = "303"},
                new Province { Nom = "Toliary", Code = "404"},
                new Province { Nom = "Antsiranana", Code = "505"},
                new Province { Nom = "Fianarantsoa", Code = "601"}
            };

            ctx.Provinces.AddRange(provinces);
            ctx.SaveChanges();
        }

        private void AddRegions(SocleDbContext ctx)
        {
            if (ctx.Regions.Any())
            {
                return;
            }

            var provinceT = ctx.Provinces.FirstOrDefault(p => p.Nom.Contains("Antananarivo"));
            var provinceF = ctx.Provinces.FirstOrDefault(p => p.Nom.Contains("Fianarantsoa"));
            var regions = new List<Region>
            {
                new Region { Nom = "Analamanga", Province = provinceT },
                new Region { Nom = "Haute Matsiatra", Province = provinceF },
                new Region { Nom = "Itasy", Province = provinceT },
                new Region { Nom = "Ankazobe", Province = provinceT },
                new Region { Nom = "Antsororokavo", Province = provinceF },
                new Region { Nom = "Anjozorobe", Province = provinceT },
            };

            ctx.Regions.AddRange(regions);
            ctx.SaveChanges();
        }

        private void AddFonctions(SocleDbContext ctx)
        {
            if (ctx.Fonctions.Any())
            {
                return;
            }

            var fonctions = new List<Fonction>
            {
                new Fonction { Nom = "Pasitera" },
                new Fonction { Nom = "Evanjelisitra" },
                new Fonction { Nom = "Laika" },
                new Fonction { Nom = "SAB" },
                new Fonction { Nom = "Mpino" },
                new Fonction { Nom = "Mpitoriteny mpanampy" },
                new Fonction { Nom = "Biraom-piangonana" },
                new Fonction { Nom = "Mpiandraikitra section" },
                new Fonction { Nom = "Mpampianatra sekoly alahady" },
            };

            ctx.Fonctions.AddRange(fonctions);
            ctx.SaveChanges();
        }

        private void InitializeData(SocleDbContext ctx)
        {
            AddProvinces(ctx);
            AddRegions(ctx);
            AddFonctions(ctx);
        }
    }
}
