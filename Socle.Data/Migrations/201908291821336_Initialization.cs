namespace Socle.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillingDetails",
                c => new
                    {
                        BillingDetailId = c.Int(nullable: false, identity: true),
                        Owner = c.String(maxLength: 255),
                        Number = c.String(maxLength: 255),
                        BankName = c.String(maxLength: 255),
                        Swift = c.String(maxLength: 255),
                        CardType = c.Int(),
                        ExpiryMonth = c.String(maxLength: 255),
                        ExpiryYear = c.String(maxLength: 255),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.BillingDetailId);
            
            CreateTable(
                "dbo.Communes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegionId = c.Int(),
                        Code = c.String(maxLength: 255),
                        Nom = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CommuneId = c.Int(),
                        Code = c.String(maxLength: 255),
                        Nom = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Communes", t => t.CommuneId)
                .Index(t => t.CommuneId);
            
            CreateTable(
                "dbo.Fokontany",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DistrictId = c.Int(),
                        Code = c.String(maxLength: 255),
                        Nom = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Districts", t => t.DistrictId)
                .Index(t => t.DistrictId);
            
            CreateTable(
                "dbo.Personnes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Actif = c.Boolean(nullable: false),
                        Age = c.Int(),
                        CIN = c.String(maxLength: 255),
                        DateBapteme = c.DateTime(),
                        DateCIN = c.DateTime(),
                        DateDeNaissance = c.DateTime(),
                        DateDuplicata = c.DateTime(),
                        IsDuplicata = c.Boolean(nullable: false),
                        LieuBapteme = c.String(maxLength: 255),
                        LieuCIN = c.String(maxLength: 255),
                        LieuDeNaissance = c.String(maxLength: 255),
                        LieuDuplicata = c.String(maxLength: 255),
                        NomComplet = c.String(maxLength: 255),
                        NumeroCarteBapteme = c.String(maxLength: 255),
                        Section = c.String(maxLength: 255),
                        FokontanyId = c.Int(),
                        FonctionId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fokontany", t => t.FokontanyId)
                .ForeignKey("dbo.Fonctions", t => t.FonctionId)
                .Index(t => t.FokontanyId)
                .Index(t => t.FonctionId);
            
            CreateTable(
                "dbo.Fonctions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Nom = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProvinceId = c.Int(nullable: false),
                        Code = c.String(maxLength: 255),
                        Nom = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Provinces", t => t.ProvinceId, cascadeDelete: true)
                .Index(t => t.ProvinceId);
            
            CreateTable(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 255),
                        Nom = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(nullable: false, maxLength: 255),
                        PhoneNumber = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        ContactId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId)
                .Index(t => t.ContactId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.People", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.Regions", "ProvinceId", "dbo.Provinces");
            DropForeignKey("dbo.Communes", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Personnes", "FonctionId", "dbo.Fonctions");
            DropForeignKey("dbo.Personnes", "FokontanyId", "dbo.Fokontany");
            DropForeignKey("dbo.Fokontany", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.Districts", "CommuneId", "dbo.Communes");
            DropIndex("dbo.People", new[] { "ContactId" });
            DropIndex("dbo.Regions", new[] { "ProvinceId" });
            DropIndex("dbo.Personnes", new[] { "FonctionId" });
            DropIndex("dbo.Personnes", new[] { "FokontanyId" });
            DropIndex("dbo.Fokontany", new[] { "DistrictId" });
            DropIndex("dbo.Districts", new[] { "CommuneId" });
            DropIndex("dbo.Communes", new[] { "RegionId" });
            DropTable("dbo.People");
            DropTable("dbo.Contacts");
            DropTable("dbo.Provinces");
            DropTable("dbo.Regions");
            DropTable("dbo.Fonctions");
            DropTable("dbo.Personnes");
            DropTable("dbo.Fokontany");
            DropTable("dbo.Districts");
            DropTable("dbo.Communes");
            DropTable("dbo.BillingDetails");
        }
    }
}
