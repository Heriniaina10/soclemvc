﻿namespace Socle.Data.Constantes
{
    public static class UtilityConstant
    {
        // IMPORT EXCEL
        public const string TableName = "TABLE_NAME";
        public const string Xls = "Xls";
        public const string ExtensionXls = ".xls";
        public const string Xlsx = "Xlsx";
        public const string ExtensionXlsx = ".xlsx";

        // OUTPUT PATH OF IMPORT EXCEL
        public const string ImportOutputPath = "~/DocTemporaires/";//"~/Content/DocFichePoste/";
    }
}
