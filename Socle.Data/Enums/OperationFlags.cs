﻿namespace Socle.Data.Enums
{
    /// <summary>
    /// Flag opéraition
    /// </summary>
    public enum OperationFlags : short
    {
        ERROR = -1,
        SUCCESS = 1,
        NOP = 0
    }
}
