﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Socle.Data.Context
{
    public class VarcharConvention : Convention
    {
        public VarcharConvention()
        {
            Properties<string>()
                .Configure(s =>
                {
                    s.HasMaxLength(255);
                    // s.HasColumnType("varchar");
                });
        }
    }
}
