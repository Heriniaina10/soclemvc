﻿using System.Data.Entity.ModelConfiguration.Conventions;
using Socle.Data.Entities;

namespace Socle.Data.Context
{
    using System.Data.Entity;
    using System.Web.Configuration;

    public class SocleDbContext : DbContext
    {
        protected static bool AlreadyInitialized;

        // Votre contexte a été configuré pour utiliser une chaîne de connexion « SocleDbContext » du fichier 
        // de configuration de votre application (App.config ou Web.config). Par défaut, cette chaîne de connexion cible 
        // la base de données « Socle.Data.Context.SocleDbContext » sur votre instance LocalDb. 
        // 
        // Pour cibler une autre base de données et/ou un autre fournisseur de base de données, modifiez 
        // la chaîne de connexion « SocleDbContext » dans le fichier de configuration de l'application.
        public SocleDbContext()
            : base("name=SocleDbContext")
        {
            if (!AlreadyInitialized)
            {
                var command = "migrate"; // WebConfigurationManager.AppSettings[ConfigConstants.KeyInitializeDatabase];
                if ("create".Equals(command))
                {
                    Database.SetInitializer(new SocleCreateIfNotExistsDatabaseInitializer());
                }
                else if ("drop-create".Equals(command))
                {
                    //Database.SetInitializer(new SocleDropCreateDatabaseInitializer());
                }
                else if ("migrate".Equals(command))
                {
                    Database.SetInitializer(new SocleMigrationInitializer());
                }
                else
                {
                    //Database.SetInitializer(new NullDatabaseInitializer<GamplDbContext>());
                }
                AlreadyInitialized = true;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new VarcharConvention());
            modelBuilder.Conventions.Add(new OneToManyCascadeDeleteConvention());
        }

        // Ajoutez un DbSet pour chaque type d'entité à inclure dans votre modèle. Pour plus d'informations 
        // sur la configuration et l'utilisation du modèle Code First, consultez http://go.microsoft.com/fwlink/?LinkId=390109.

         public virtual DbSet<Person> Persons { get; set; }
         public virtual DbSet<Contact> Contacts { get; set; }

         public DbSet<BillingDetail> BillingDetails { get; set; }

        /**********************************************************************************************/

        public virtual DbSet<Personne> Personnes { get; set; }
        public virtual DbSet<Fokontany> Fokontany { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Commune> Communes { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<Fonction> Fonctions { get; set; }


    }
}