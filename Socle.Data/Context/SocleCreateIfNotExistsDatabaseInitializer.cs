﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Socle.Data.Context
{
    internal class SocleCreateIfNotExistsDatabaseInitializer : CreateDatabaseIfNotExists<SocleDbContext>
    {
        protected override void Seed(SocleDbContext context)
        {
            //DbContextHelper.InitDb<GamplScriptSeedInitializer, SocleDbContext>(context);

            base.Seed(context);
        }
    }
}
