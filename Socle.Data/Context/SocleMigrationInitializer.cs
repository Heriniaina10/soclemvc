﻿using System.Data.Entity;
using Socle.Data.Migrations;

namespace Socle.Data.Context
{
    internal class SocleMigrationInitializer : MigrateDatabaseToLatestVersion<SocleDbContext, Configuration>
    {}
}
