﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Repository.Fokontany;

namespace Socle.Service.Fokontany
{
    public sealed class FokontanyService : GenericService<Data.Entities.Fokontany, SocleDbContext, FokontanyRepository, FokontanyService>, IFokontanyService
    {
    }
}
