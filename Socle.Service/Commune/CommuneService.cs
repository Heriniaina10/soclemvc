﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Repository.Commune;

namespace Socle.Service.Commune
{
    public sealed class CommuneService : GenericService<Data.Entities.Commune, SocleDbContext, CommuneRepository, CommuneService>, ICommuneService
    {
    }
}
