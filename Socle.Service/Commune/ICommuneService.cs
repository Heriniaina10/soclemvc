﻿using Application.Etech.Core.Services;

namespace Socle.Service.Commune
{
    public interface ICommuneService : IService<Data.Entities.Commune>
    {
    }
}
