﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Etech.Core.Services;

namespace Socle.Service.Person
{
    interface IPersonService : IService<Data.Entities.Person>
    {
        void Andrana();
    }
}
