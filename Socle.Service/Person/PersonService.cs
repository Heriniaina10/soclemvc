﻿using Socle.Data.Context;
using Application.Etech.Core.Services;
using Socle.Repository.Person;

namespace Socle.Service.Person
{
    public class PersonService : GenericService<Data.Entities.Person, SocleDbContext, PersonRepository, PersonService>, IPersonService
    {
        public void Andrana()
        {
            Instance.Repository.TestLogger();
        }
    }
}
