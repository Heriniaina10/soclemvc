﻿using Application.Etech.Core.Services;

namespace Socle.Service.Province
{
    public interface IProvinceService : IService<Data.Entities.Province>
    {
    }
}
