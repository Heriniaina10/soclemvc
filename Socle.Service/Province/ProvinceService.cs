﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Repository.Province;

namespace Socle.Service.Province
{
    public sealed class ProvinceService : GenericService<Data.Entities.Province, SocleDbContext, ProvinceRepository, ProvinceService>, IProvinceService
    {
    }
}
