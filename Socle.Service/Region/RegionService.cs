﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Repository.Region;

namespace Socle.Service.Region
{
    public sealed class RegionService : GenericService<Data.Entities.Region, SocleDbContext, RegionRepository, RegionService>, IRegionService
    {
    }
}
