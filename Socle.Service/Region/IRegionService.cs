﻿using Application.Etech.Core.Services;

namespace Socle.Service.Region
{
    public interface IRegionService : IService<Data.Entities.Region>
    {
    }
}
