﻿using Application.Etech.Core.Services;

namespace Socle.Service.Personne
{
    interface IPersonneService : IService<Data.Entities.Personne>
    {}
}
