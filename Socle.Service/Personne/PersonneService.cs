﻿using Socle.Data.Context;
using Application.Etech.Core.Services;
using Socle.Repository.Personne;

namespace Socle.Service.Personne
{
    public class PersonneService : GenericService<Data.Entities.Personne, SocleDbContext, PersonneRepository, PersonneService>, IPersonneService
    {}
}
