﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Repository.Fonction;

namespace Socle.Service.Fonction
{
    public sealed class FonctionService : GenericService<Data.Entities.Fonction, SocleDbContext, FonctionRepository, FonctionService>, IFonctionService
    {
    }
}
