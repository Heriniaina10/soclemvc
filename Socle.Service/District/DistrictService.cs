﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Repository.District;

namespace Socle.Service.District
{
    public sealed class DistrictService : GenericService<Data.Entities.District, SocleDbContext, DistrictRepository, DistrictService>, IDistrictService
    {
    }
}
