﻿using Application.Etech.Core.Services;

namespace Socle.Service.District
{
    public interface IDistrictService : IService<Data.Entities.District>
    {
    }
}
