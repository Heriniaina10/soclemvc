﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Data.Dto;
using Socle.Repository.Province;
using Socle.Service.Province;

namespace Socle.Applicatif.Province
{
    public sealed class ProvinceServiceApp : GenericServiceApp<Data.Entities.Province, ProvinceDto, SocleDbContext, ProvinceRepository, ProvinceService, ProvinceServiceApp>, IProvinceServiceApp
    {
    }
}
