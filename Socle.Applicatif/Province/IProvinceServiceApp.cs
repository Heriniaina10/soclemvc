﻿using Application.Etech.Core.Services;
using Socle.Data.Dto;

namespace Socle.Applicatif.Province
{
    public interface IProvinceServiceApp : IServiceApp<Data.Entities.Province, ProvinceDto>
    {
    }
}
