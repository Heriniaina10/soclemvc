﻿using Application.Etech.Core.Services;
using Socle.Data.Dto.Personne;

namespace Socle.Applicatif.Personne
{
    interface IPersonneApplicatif : IServiceApp<Data.Entities.Personne, PersonneDto>
    {}
}
