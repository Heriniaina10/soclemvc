﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Data.Dto.Personne;
using Socle.Repository.Personne;
using Socle.Service.Personne;

namespace Socle.Applicatif.Personne
{
    public class PersonneApplicatif : GenericServiceApp<Data.Entities.Personne, PersonneDto, SocleDbContext, PersonneRepository, PersonneService, PersonneApplicatif>
    {}
}
