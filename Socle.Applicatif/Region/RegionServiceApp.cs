﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Data.Dto;
using Socle.Repository.Region;
using Socle.Service.Region;

namespace Socle.Applicatif.Region
{
    public sealed class RegionServiceApp : GenericServiceApp<Data.Entities.Region, RegionDto, SocleDbContext, RegionRepository, RegionService, RegionServiceApp>, IRegionServiceApp
    {
    }
}
