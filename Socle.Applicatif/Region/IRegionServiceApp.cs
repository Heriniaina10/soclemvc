﻿using Application.Etech.Core.Services;
using Socle.Data.Dto;

namespace Socle.Applicatif.Region
{
    public interface IRegionServiceApp : IServiceApp<Data.Entities.Region, RegionDto>
    {
    }
}
