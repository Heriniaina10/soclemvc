﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Etech.Core.Services;
using Socle.Data;
using Socle.Data.Dto;
using Socle.Data.Dto.Personne;

namespace Socle.Applicatif.Person
{
    interface IPersonApplicatif : IServiceApp<Data.Entities.Person, PersonDto>
    {
    }
}
