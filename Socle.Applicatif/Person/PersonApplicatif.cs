﻿using Application.Etech.Core.Services;
using Socle.Data;
using Socle.Data.Context;
using Socle.Data.Dto;
using Socle.Data.Dto.Personne;
using Socle.Repository.Person;
using Socle.Service.Person;

namespace Socle.Applicatif.Person
{
    public class PersonApplicatif : GenericServiceApp<Data.Entities.Person, PersonDto, SocleDbContext, PersonRepository, PersonService, PersonApplicatif>
    {
    }
}
