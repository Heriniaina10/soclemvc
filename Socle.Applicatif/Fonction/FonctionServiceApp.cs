﻿using Application.Etech.Core.Services;
using Socle.Data.Context;
using Socle.Data.Dto;
using Socle.Repository.Fonction;
using Socle.Service.Fonction;

namespace Socle.Applicatif.Fonction
{
    public sealed class FonctionServiceApp : GenericServiceApp<Data.Entities.Fonction, FonctionDto, SocleDbContext, FonctionRepository, FonctionService, FonctionServiceApp>, IFonctionServiceApp
    {
    }
}
