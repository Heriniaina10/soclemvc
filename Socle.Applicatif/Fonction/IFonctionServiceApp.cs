﻿using Application.Etech.Core.Services;
using Socle.Data.Dto;

namespace Socle.Applicatif.Fonction
{
    public interface IFonctionServiceApp : IServiceApp<Data.Entities.Fonction, FonctionDto>
    {
    }
}
