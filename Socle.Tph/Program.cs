﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Socle.Data.Context;
using Socle.Data.Entities;

namespace Socle.Tph
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new SocleDbContext())
            {
                //AddBankAccount(ctx);
                //AddCard(ctx);
                //ctx.SaveChanges();

                //IQueryable<CreditCard> query = from b in 
                //                                ctx.BillingDetails.OfType<CreditCard>()
                //                                select b;
                var query = ctx.BillingDetails.OfType<CreditCard>().ToList();

                var bilings = query.ToList();
            }
        }

        static void AddBankAccount(SocleDbContext ctx)
        {
            var biling = new BankAccount
            {
                BankName = "abm",
                Number = "accès bank",
                Owner = "owner2",
                Swift = "swift1"
            };

            ctx.BillingDetails.Add(biling);
        }

        static void AddCard(SocleDbContext ctx)
        {
            var biling = new CreditCard
            {
                Number = "accès bank",
                Owner = "card owner",
                CardType = 1,
                ExpiryMonth = "Juin",
                ExpiryYear = "2019"
            };

            ctx.BillingDetails.Add(biling);

        }

        static IQueryable<T> QueryNotPolymorph<T>(SocleDbContext ctx)
        {
            IQueryable<T> query = from b in
                                  ctx.BillingDetails.OfType<T>()
                                  select b;
            return query;
        }
    }
}
