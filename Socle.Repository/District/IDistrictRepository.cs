﻿using Application.Etech.Core.Repository;

namespace Socle.Repository.District
{
    public interface IDistrictRepository : IRepository<Data.Entities.District>
    {}
}
