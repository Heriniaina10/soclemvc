﻿using Application.Etech.Core.Repository;
using Socle.Data.Context;

namespace Socle.Repository.District
{
    public class DistrictRepository : GenericRepository<Data.Entities.District, SocleDbContext, DistrictRepository>, IDistrictRepository
    {
    }
}
