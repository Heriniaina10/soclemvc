﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Etech.Core.Repository;

namespace Socle.Repository.Person
{
    interface IPersonRepository : IRepository<Data.Entities.Person>
    {
    }
}
