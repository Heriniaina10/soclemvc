﻿using Application.Etech.Core.Repository;
using Socle.Data.Context;

namespace Socle.Repository.Province
{
    public class ProvinceRepository : GenericRepository<Data.Entities.Province, SocleDbContext, ProvinceRepository>, IProvinceRepository
    {
    }
}
