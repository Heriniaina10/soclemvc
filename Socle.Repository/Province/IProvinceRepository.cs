﻿using Application.Etech.Core.Repository;

namespace Socle.Repository.Province
{
    public interface IProvinceRepository : IRepository<Data.Entities.Province>
    {}
}
