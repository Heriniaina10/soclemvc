﻿using Application.Etech.Core.Repository;

namespace Socle.Repository.Personne
{
    interface IPersonneRepository : IRepository<Data.Entities.Personne>
    {
    }
}
