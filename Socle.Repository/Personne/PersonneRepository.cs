﻿using Application.Etech.Core.Repository;
using Socle.Data.Context;

namespace Socle.Repository.Personne
{
    public class PersonneRepository : GenericRepository<Data.Entities.Personne, SocleDbContext, PersonneRepository>, IPersonneRepository
    {}
}
