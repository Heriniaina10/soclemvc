﻿using Application.Etech.Core.Repository;

namespace Socle.Repository.Fonction
{
    public interface IFonctionRepository : IRepository<Data.Entities.Fonction>
    {}
}
