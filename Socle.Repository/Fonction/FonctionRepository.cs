﻿using Application.Etech.Core.Repository;
using Socle.Data.Context;

namespace Socle.Repository.Fonction
{
    public class FonctionRepository : GenericRepository<Data.Entities.Fonction, SocleDbContext, FonctionRepository>, IFonctionRepository
    {
    }
}
