﻿using Application.Etech.Core.Repository;
using Socle.Data.Context;

namespace Socle.Repository.Region
{
    public class RegionRepository : GenericRepository<Data.Entities.Region, SocleDbContext, RegionRepository>, IRegionRepository
    {
    }
}
