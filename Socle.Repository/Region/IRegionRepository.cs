﻿using Application.Etech.Core.Repository;

namespace Socle.Repository.Region
{
    public interface IRegionRepository : IRepository<Data.Entities.Region>
    {}
}
