﻿using Application.Etech.Core.Repository;
using Socle.Data.Context;

namespace Socle.Repository.Fokontany
{
    public class FokontanyRepository : GenericRepository<Data.Entities.Fokontany, SocleDbContext, FokontanyRepository>, IFokontanyRepository
    {
    }
}
