﻿using Application.Etech.Core.Repository;

namespace Socle.Repository.Fokontany
{
    public interface IFokontanyRepository : IRepository<Data.Entities.Fokontany>
    {}
}
