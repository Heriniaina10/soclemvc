﻿using Application.Etech.Core.Repository;

namespace Socle.Repository.Commune
{
    public interface ICommuneRepository : IRepository<Data.Entities.Commune>
    {}
}
