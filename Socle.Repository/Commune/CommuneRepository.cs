﻿using Application.Etech.Core.Repository;
using Socle.Data.Context;

namespace Socle.Repository.Commune
{
    public class CommuneRepository : GenericRepository<Data.Entities.Commune, SocleDbContext, CommuneRepository>, ICommuneRepository
    {
    }
}
