﻿using System;
using Mapster;
using Socle.Data.Dto.Personne;
using Socle.Data.Entities;

namespace Socle.Constraints.Mappings
{
    public class PersonneMapping : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Personne, PersonneDto>();
            config.NewConfig<PersonneDto, Personne>();

            config.NewConfig<Personne, PersonneCsvDto>();
            config.NewConfig<PersonneCsvDto, Personne>()
                .Ignore(item => item.Id)
                //.Map(dest => dest.Id, src => new Guid())
                .Map(dest => dest.DateDeNaissance, src => Convert.ToDateTime(src.DateDeNaissance), cond => !string.IsNullOrEmpty(cond.DateDeNaissance))
                .Map(dest => dest.DateBapteme, src => Convert.ToDateTime(src.DateBapteme), cond => !string.IsNullOrEmpty(cond.DateBapteme))
                .Map(dest => dest.DateCIN, src => Convert.ToDateTime(src.DateCIN), cond => !string.IsNullOrEmpty(cond.DateCIN))
                .Map(dest => dest.DateDuplicata, src => Convert.ToDateTime(src.DateDuplicata), cond => !string.IsNullOrEmpty(cond.DateDuplicata));
        }
    }
}
