﻿using Mapster;
using Socle.Data.Dto.Personne;
using Socle.Data.Entities;

namespace Socle.Constraints.Mappings
{
    public class PersonMapping : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Person, PersonDto>();
            config.NewConfig<PersonDto, Person>();
        }
    }
}
