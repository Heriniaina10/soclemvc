﻿using Socle.Applicatif.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace SocleMvc.WebApi.Controllers.Api
{
    [System.Web.Http.Route("api/persons")]
    public class PersonsController : ApiController
    {
        //[ResponseType(typeof(Person))]
        public IHttpActionResult GetAll()
        {
            var persons = PersonApplicatif.Instance.FindAll();

            return Ok(persons);
        }
    }
}