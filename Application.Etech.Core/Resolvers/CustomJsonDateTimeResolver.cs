﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Application.Etech.Core.Resolvers
{
    /// <summary>
    /// Custom resolver pour Date Fr et DateTime Fr
    /// </summary>
    public class CustomJsonDateTimeResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            // skip if the property is not a DateTime
            if (property.PropertyType != typeof(DateTime))
            {
                return property;
            }

            var attr = (DisplayFormatAttribute)member.GetCustomAttributes(typeof(DisplayFormatAttribute), true).SingleOrDefault();
            if (attr != null)
            {
                var converter = new IsoDateTimeConverter();
                converter.DateTimeFormat = attr.DataFormatString;
                property.Converter = converter;
            }

            return property;
        }

    }

    //public class Mia : DefaultSerializationBinder
    //{
    //    public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
    //    {
    //        base.BindToName(serializedType, out assemblyName, out typeName);
    //    }
    //}
}