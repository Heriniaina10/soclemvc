using System;
using System.Linq.Expressions;

namespace Application.Etech.Core.Searchs
{
    public interface ISearch<TEntity>
    {
        bool IsEmpty();
        Expression<Func<TEntity, bool>> ToPredicate();
    }
}