﻿using System;
using System.Configuration;
using System.Reflection;
using log4net.Config;
using log4net.Repository;

namespace Application.Etech.Core.Log4net
{
    [AttributeUsage(AttributeTargets.Assembly)]
    [Serializable]
    public class EtechXmlConfiguratorAttribute : XmlConfiguratorAttribute

    {
        /// <summary>
        /// Log base directory in appSettings
        /// </summary>
        public bool PathInAppSetting { get; set; } = false;
        /// <summary>
        /// Default to "log4net.Directory"
        /// </summary>
        public string PathKey { get; set; } = "log4net.Directory";
        public override void Configure(Assembly sourceAssembly, ILoggerRepository targetRepository)
        {
            if (PathInAppSetting && !string.IsNullOrEmpty(PathKey))
            {
                var logDirectory = ConfigurationManager.AppSettings[PathKey];
                if (!string.IsNullOrWhiteSpace(logDirectory) && !logDirectory.EndsWith("\\"))
                {
                    logDirectory = $"{logDirectory}\\";
                }
                log4net.GlobalContext.Properties["LogDirectory"] = logDirectory ?? "";
            }

            base.Configure(sourceAssembly, targetRepository);
        }
    }
}