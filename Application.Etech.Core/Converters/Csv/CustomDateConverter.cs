﻿using System;
using System.Globalization;
using FileHelpers;

namespace Application.Etech.Core.Converters.Csv
{
    public class CustomDateConverter : ConverterBase
    {
        private readonly string _format;
        private readonly CultureInfo _culture;
        private readonly string _field;

        /// <summary>
        /// Convert a value to a date time value
        /// 
        /// </summary>
        public CustomDateConverter(string field): this(DefaultDateTimeFormat, field)
        {
            
        }

        /// <summary>
        /// Convert a value to a date time value
        /// 
        /// </summary>
        /// <param name="format">date format see .Net documentation</param>
        public CustomDateConverter(string format, string field): this(format, field, null)
      {
        }

        /// <summary>
        /// Convert a value to a date time value
        /// 
        /// </summary>
        /// <param name="format">date format see .Net documentation</param><param name="culture">The culture used to parse the Dates</param>
        public CustomDateConverter(string format,string field, string culture)
        {
            this._format = format;
            _field = field;
            if (culture == null)
                return;
            this._culture = CultureInfo.GetCultureInfo(culture);
        }

        /// <summary>
        /// Convert a string to a date time value
        /// 
        /// </summary>
        /// <param name="from">String value to convert</param>
        /// <returns>
        /// DateTime value
        /// </returns>
        public override object StringToField(string from)
        {
            if (from == null)
                from = string.Empty;
            DateTime result;
            if (!DateTime.TryParseExact(from.Trim(), this._format, this._culture, DateTimeStyles.None, out result))
            {
                var extraInfo = string.Format("==>Le champ '{0}' doit être au format '{1}'.", _field, _format);
                throw new ConvertException(from,typeof(DateTime),extraInfo);
            }
            return (object)result;
        }

        /// <summary>
        /// Convert a date time value to a string
        /// 
        /// </summary>
        /// <param name="from">DateTime value to convert</param>
        /// <returns>
        /// string DateTime value
        /// </returns>
        public override string FieldToString(object from)
        {
            if (from == null)
                return string.Empty;
            return Convert.ToDateTime(from).ToString(this._format, this._culture);
        }
    }
}