﻿using FileHelpers;

namespace Application.Etech.Core.Converters.Csv
{
    public class CustomFieldNotEmpty : ConverterBase
    {
        private readonly string _field;
        public CustomFieldNotEmpty(string field)
        {
            _field = field;
        }
        public override object StringToField(string from)
        {
            var val = (from ??"").Trim();
            if(string.IsNullOrEmpty(val)) throw  new ConvertException(from, typeof(string), string.Format("==>La colonne '{0}' ne peut pas être null ou vide.", _field));
            return from;
        }
    }
}