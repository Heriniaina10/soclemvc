﻿using System;
using System.Globalization;
using FileHelpers;

namespace Application.Etech.Core.Converters.Csv
{
    public class CustomNumberConverter : ConverterBase
    {
        private readonly CultureInfo _culture;
        private readonly string _field;
        private readonly CustomNumberTypes _type;

        /// <summary>
        /// Convert to a type given a decimal separator
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <param name="type">type we are converting</param><param name="decimalSep">Separator</param>
        protected CustomNumberConverter(string field, CustomNumberTypes type=CustomNumberTypes.Int,string decimalSep=".")
        {
            _type = type;
            _field = field;
            this._culture = CreateCultureInfo(decimalSep);
        }

        /// <summary>
        /// Convert the field to a string representation
        /// 
        /// </summary>
        /// <param name="from">Object to convert</param>
        /// <returns>
        /// string representation
        /// </returns>
        public sealed override string FieldToString(object from)
        {
            if (from == null)
                return string.Empty;
            return ((IConvertible)from).ToString(this._culture);
        }

        /// <summary>
        /// Convert a string to the object type
        /// 
        /// </summary>
        /// <param name="from">String to convert</param>
        /// <returns>
        /// Object converted to
        /// </returns>
        public override sealed object StringToField(string from)
        {
            object result=0;
            try
            {
                var fromVal = from ?? "".Trim();
                switch (_type)
                {
                    case CustomNumberTypes.Int:
                        result = Convert.ToInt32(fromVal, _culture);
                        break;
                    case CustomNumberTypes.Double:
                        result = Convert.ToDouble(fromVal, _culture);
                        break;
                    case CustomNumberTypes.Float:
                        result = Convert.ToDouble(fromVal, _culture);
                        break;
                    case CustomNumberTypes.NullableInt:
                        result = string.IsNullOrEmpty(fromVal) ? (int?)null : Convert.ToInt32(fromVal, _culture);
                        break;
                    case CustomNumberTypes.NullableDouble:
                        result = string.IsNullOrEmpty(fromVal)?(double?) null : Convert.ToDouble(fromVal, _culture);
                        break;
                    case CustomNumberTypes.NullableFloat:
                        result = string.IsNullOrEmpty(fromVal) ? (decimal?)null : Convert.ToDecimal(fromVal, _culture);
                        break;
                }
            }
            catch (Exception)
            {
                throw new ConvertException(from, typeof(DateTime), string.Format("==>Le champ '{0}' doit être de type numérique.", _field));
            }

            return result;
        }

        private CultureInfo CreateCultureInfo(string decimalSep=".")
        {
            CultureInfo cultureInfo = new CultureInfo(CultureInfo.CurrentCulture.LCID);
            if (decimalSep == ".")
            {
                cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
                cultureInfo.NumberFormat.NumberGroupSeparator = ",";
            }
            else
            {
                if (!(decimalSep == ","))
                    throw new FileHelpersException("Utilisez seulement ces valeur '.' ou ',' comme séparateur de millier ou séparateur de décimal");
                cultureInfo.NumberFormat.NumberDecimalSeparator = ",";
                cultureInfo.NumberFormat.NumberGroupSeparator = ".";
            }
            return cultureInfo;
        }
    }

    public enum CustomNumberTypes
    {
        Double,
        Int,
        Float,
        NullableDouble,
        NullableInt,
        NullableFloat
    }
}