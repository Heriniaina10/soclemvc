﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using Dapper;

namespace Application.Etech.Core.Repository
{
    /// <summary>
    /// Interface repository
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <author>Fenonantenaina</author>
    public interface IRepository<TEntity> where TEntity : class
    {
        DbConnection CurrentConnection { get; }
        DbSet<TEntity> Entities { get; }

        /// <summary>
        /// Créer une nouvelle instance de Entité rattachée au context courant 
        /// </summary>
        TEntity New();
        /// <summary>
        /// Ajouter un ou plusieurs entités dans la base
        /// </summary>
        /// <param name="entity"></param>
        void Add(params TEntity[] entity);

        /// <summary>
        /// Attacher un ou plusieurs entités dans le context courant
        /// </summary>
        /// <param name="entity"></param>
        void Attach(params TEntity[] entity);
        /// <summary>
        /// Update un ou plusieurs entités
        /// </summary>
        /// <param name="entity"></param>
        void Update(params TEntity[] entity);

        /// <summary>
        /// Supprimer un ou plusieurs entités
        /// </summary>
        /// <param name="entity"></param>
        void Delete(params TEntity[] entity);

        /// <summary>
        /// Delete an entity by id
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(object id);

        /// <summary>
        /// Récupérer tous les enregistrements selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate = null);
        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<TModel> FindAll<TModel>(Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null);
        /// <summary>
        /// Récupérer tous les enregistrements avec une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="sortField"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowCount = 10, bool orderAsc = true);
        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur et une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="sortField"></param>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        IEnumerable<TModel> FindAllWithPagination<TModel>(string sortField, Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowCount = 10, bool orderAsc = true);
        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> selon le critère [predicate] (si non null).
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> predicate = null);
        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> avec une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        IQueryable<TEntity> QueryWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowCount = 10, bool orderAsc = true);
        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> avec un selecteur selon le critère [predicate] (si non null).
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryable<TModel> QuerySelector<TModel>(Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null);
        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> avec un selecteur et une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sortField"></param>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        IQueryable<TModel> QuerySelectorWithPagination<TModel>(string sortField, Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowCount = 10, bool orderAsc = true);
        /// <summary>
        /// Récupère un enregistrement depuis la base par son id
        /// </summary>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        TEntity Find(params object[] keyValues);
        /// <summary>
        /// Récupère le premier enregistrement qui satisfait le critère [predicate]
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Null si aucun enregistrement trouvé</returns>
        TEntity First(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Récupère le premier enregistrement qui satisfait le critère [predicate]
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>La valeur default() si aucun enregistrement trouvé</returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Retourne le nombre d'enregistrement correspond au critère [predicate].
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        int Count(Expression<Func<TEntity, bool>> predicate = null);

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        DbRawSqlQuery<TEntity> SqlQuery(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        DbRawSqlQuery<TModel> SqlQuery<TModel>(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        void ExecuteNoQuery(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Delete all rows
        /// </summary>
        void DeleteAll();

        #region DAPPER

        IEnumerable<TModel> DapperQuery<TModel>(string sqlQuery, object param = null);

        TModel DapperSingleQuery<TModel, TInner>(string sqlQuery, Func<TModel, TInner, TModel> map, object param = null,
            string splitOn = "Id");
        
        TModel DapperSingleQuery<TModel>(string sqlQuery, object param = null);

        TModel DapperSingleQuery<TModel, TInnerFirst, TInnerSecond>(string sqlQuery,
            Func<TModel, TInnerFirst, TInnerSecond, TModel> map, object param = null, string splitOn = "Id");
        
        int DapperExecute(string sqlQuery, object param = null);

        #endregion

    }
}