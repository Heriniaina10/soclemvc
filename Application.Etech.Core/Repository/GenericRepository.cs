﻿using Application.Etech.Core.Extensions;
using Application.Etech.Core.Paginations;
using Application.Etech.Core.Searchs;
using Dapper;
using Mehdime.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Application.Etech.Core.Repository
{
    /// <summary>
    /// Repository generique, à utiliser seulement à l'intérieur d'un scope DbContextScope
    /// </summary>
    /// <typeparam name="TEntity">Entite</typeparam>
    /// <typeparam name="TContext">Context</typeparam>
    /// <typeparam name="TRepository">Repository</typeparam>
    /// <author>Fenonantenaina</author>
    public abstract class GenericRepository<TEntity, TContext, TRepository> : IRepository<TEntity> 
        where TEntity : class
        where TContext :  DbContext 
        where TRepository : IRepository<TEntity>
    {
        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(TRepository));
        private readonly Lazy<IAmbientDbContextLocator> _locator = new Lazy<IAmbientDbContextLocator>(() => new AmbientDbContextLocator());
        private static readonly Lazy<TRepository> _instance = new Lazy<TRepository>(() => (TRepository)Activator.CreateInstance(typeof(TRepository), true));

        protected GenericRepository() { }

        public static  TRepository Instance => _instance.Value;

        protected IAmbientDbContextLocator DbContextLocator => _locator.Value;

        public TContext CurrentContext
        {
            get
            {
                var dbContext = DbContextLocator.Get<TContext>();

                if (dbContext == null)                    
                    throw new InvalidOperationException("No ambient DbContext of type UserManagementDbContext found. This means that this repository method has been called outside of the scope of a DbContextScope. A repository must only be accessed within the scope of a DbContextScope, which takes care of creating the DbContext instances that the repositories need and making them available as ambient contexts. This is what ensures that, for any given DbContext-derived type, the same instance is used throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory in your top-level business logic service method to create a DbContextScope that wraps the entire business transaction that your service method implements. Then access this repository within that scope. Refer to the comments in the IDbContextScope.cs file for more details.");

                return dbContext;
            }
        }

        public DbConnection CurrentConnection => CurrentContext.Database.Connection;


        #region MAJ ENREGISTREMENT & BDD

        public TEntity New()
        {
            return CurrentContext.Set<TEntity>().Create();
        }

        /// <summary>
        /// Ajouter un ou plusieurs entités dans la base
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Add(params TEntity[] entity)
        {
            if (entity != null)
            {
               CurrentContext.Set<TEntity>().AddRange(entity);
            }
        }

        /// <summary>
        /// Attacher un ou plusieurs entités dans le context courant
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Attach(params TEntity[] entity)
        {
            if (entity != null)
            {
                var context = CurrentContext;
                foreach (var entite in entity)
                {
                    context.Set<TEntity>().Attach(entite);
                }
            }
        }

        /// <summary>
        /// Update un ou plusieurs entités
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(params TEntity[] entity)
        {
            if (entity != null)
            {
                var context = CurrentContext;
                foreach (var entite in entity)
                {
                    if (CurrentContext.Entry(entite).State == EntityState.Detached)
                    {
                        context.Set<TEntity>().Attach(entite);
                        context.Entry(entite).State = EntityState.Modified;
                    }
                }
            }
        }

        /// <summary>
        /// Supprimer un ou plusieurs entités
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Delete(params TEntity[] entity)
        {
            if (entity != null)
            {
                var context = CurrentContext;
                //On attache l'entité chargé par un autre context
                foreach (var entite in entity)
                {
                    if (CurrentContext.Entry(entite).State == EntityState.Detached)
                    {
                        context.Set<TEntity>().Attach(entite);
                    }
                }
                context.Set<TEntity>().RemoveRange(entity);
            }
        }

        public virtual void DeleteById(object id)
        {
            var entite = Find(id);
            if (entite != null)
            {
                CurrentContext.Set<TEntity>().Remove(entite);
            }
        }

        /// <summary>
        /// Delete all rows
        /// </summary>
        public virtual void DeleteAll()
        {
            CurrentContext.Set<TEntity>().RemoveRange(CurrentContext.Set<TEntity>());
        }

        #endregion

        #region CHARGEMENT LISTE EN MEMOIRE

        /// <summary>
        /// Récupérer tous les enregistrements selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate = null)
        {
            return Query(predicate).ToList(); 
        }

        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IEnumerable<TModel> FindAll<TModel>(Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null)
        {
            if (selector == null) throw new Exception("Le [selector] ne peut pas être null");
            return QuerySelector(selector,predicate).ToList(); 
        }

        /// <summary>
        /// Récupérer tous les enregistrements avec une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> FindAllWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowCount = 10, bool orderAsc = true)
        {
            return QueryWithPagination(sortField,predicate, page, rowCount,orderAsc).ToList();
        }

        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur et une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sortField"></param>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        public virtual IEnumerable<TModel> FindAllWithPagination<TModel>(string sortField, Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null, int page = 1,
            int rowCount = 10, bool orderAsc = true)
        {
            return QuerySelectorWithPagination(sortField,selector,predicate, page, rowCount,orderAsc).ToList();
        }

        #endregion

        #region QUERYABLE


        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> selon le critère [predicate] (si non null).
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> predicate = null)
        {
            
                return predicate == null
                    ? CurrentContext.Set<TEntity>().AsNoTracking()
                    : CurrentContext.Set<TEntity>().AsNoTracking().Where(predicate);
               
            
        }

        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> avec une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> QueryWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowCount = 10, bool orderAsc = true)
        {
            if (sortField == null) throw new Exception("[sortField] ne peut pas être null");
            var offset = page <= 1 ? 0 : page - 1;
            var row = rowCount <= 0 ? 10 : rowCount;
            return (predicate == null ? CurrentContext.Set<TEntity>().AsNoTracking() : CurrentContext.Set<TEntity>().AsNoTracking().Where(predicate)).CustomOrderBy(sortField,orderAsc).Skip(offset * row).Take(row);
        }

        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> avec un selecteur selon le critère [predicate] (si non null).
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IQueryable<TModel> QuerySelector<TModel>(Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null)
        {
            if (selector == null) throw new Exception("Le [selector] ne peut pas être null");
            return Query(predicate).Select(selector);
        }

        /// <summary>
        /// Récupérer un objet <see cref="IQueryable{T}"/> avec un selecteur et une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Aucun enregistrement n'est chargé en mémoire à cette instant</para>
        /// <para>A utiliser si on veut manipuler la requêtte avant de charger les enregistrements en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sortField"></param>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowCount"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        public virtual IQueryable<TModel> QuerySelectorWithPagination<TModel>(string sortField, Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowCount = 10, bool orderAsc = true)
        {
            if (sortField == null) throw new Exception("[sortField] ne peut pas être null");
            if (selector == null) throw new Exception("Le [selector] ne peut pas être null");
            return QueryWithPagination(sortField,predicate, page, rowCount,orderAsc).Select(selector);
        }

        #endregion

        #region CHARGEMENT UN ENREGISTREMENT EN MEMOIRE

        /// <summary>
        /// Récupère un enregistrement depuis la base par son id
        /// </summary>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public virtual TEntity Find(params object[] keyValues)
        {
            return CurrentContext.Set<TEntity>().Find(keyValues);
        }

        /// <summary>
        /// Récupère le premier enregistrement qui satisfait le critère [predicate]
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Null si aucun enregistrement trouvé</returns>
        public virtual TEntity First(Expression<Func<TEntity, bool>> predicate)
        {
            return CurrentContext.Set<TEntity>().First(predicate);
        }

        /// <summary>
        /// Récupère le premier enregistrement qui satisfait le critère [predicate]
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>La valeur default() si aucun enregistrement trouvé</returns>
        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return CurrentContext.Set<TEntity>().FirstOrDefault(predicate);
        }

        #endregion

        /// <summary>
        /// Retourne le nombre d'enregistrement correspond au critère [predicate].
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual int Count(Expression<Func<TEntity, bool>> predicate = null)
        {
            return predicate == null ? CurrentContext.Set<TEntity>().Count() : CurrentContext.Set<TEntity>().Count(predicate);
        }

        /// <summary>
        /// Entités [Table]
        /// </summary>
        public DbSet<TEntity> Entities => CurrentContext.Set<TEntity>();

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public virtual DbRawSqlQuery<TEntity> SqlQuery(string sqlOrProcedure, params SqlParameter[] args)
        {
            return CurrentContext.Database.SqlQuery<TEntity>(sqlOrProcedure, args);
        }

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public virtual DbRawSqlQuery<TModel> SqlQuery<TModel>(string sqlOrProcedure, params SqlParameter[] args)
        {
            return CurrentContext.Database.SqlQuery<TModel>(sqlOrProcedure, args);
        }

        /// <summary>
        /// Execute sql command
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        public virtual void ExecuteNoQuery(string sqlOrProcedure, params SqlParameter[] args)
        {
            CurrentContext.Database.ExecuteSqlCommand(sqlOrProcedure, args);
        }

        #region DAPPER

        /// <summary>
        /// Dapper Sql Query with selector
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sqlQuery"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public virtual IEnumerable<TModel> DapperQuery<TModel>(string sqlQuery, object param=null)
        {
            return CurrentContext.Database.Connection.Query<TModel>(sqlQuery, param);
        }

        public virtual TModel DapperSingleQuery<TModel, TInner>(string sqlQuery, Func<TModel, TInner, TModel> map, object param = null, string splitOn = "Id")
        {
            return CurrentContext.Database.Connection.Query<TModel, TInner, TModel>(sqlQuery, map, param, splitOn: splitOn).FirstOrDefault();
        }
        
        public virtual TModel DapperSingleQuery<TModel>(string sqlQuery, object param = null)
        {
            return CurrentContext.Database.Connection.Query<TModel>(sqlQuery, param).FirstOrDefault();
        }

        public virtual TModel DapperSingleQuery<TModel, TInnerFirst, TInnerSecond>(string sqlQuery, Func<TModel, TInnerFirst,TInnerSecond, TModel> map, object param = null, string splitOn = "Id")
        {
            return CurrentContext.Database.Connection.Query<TModel, TInnerFirst, TInnerSecond, TModel>(sqlQuery, map, param, splitOn: splitOn).FirstOrDefault();
        }

        public virtual int DapperExecute(string sqlQuery, object param = null)
        {
            return CurrentContext.Database.Connection.Execute(sqlQuery, param);
        }

        public virtual DynamicParameters BuildDapperParameters(Object obj)
        {
            DynamicParameters parameters = new DynamicParameters();
            Type type = obj.GetType();

            foreach (PropertyInfo prop in type.GetProperties())
            {
                DbType dbType = DbType.Int32;
                if (prop.PropertyType == typeof(string))
                    dbType = DbType.String;
                else if (prop.PropertyType == typeof(DateTime))
                    dbType = DbType.DateTime;

                parameters.Add(string.Format("{0}{1}", "@", prop.Name), prop.GetValue(obj, null), dbType, ParameterDirection.Input);
            }
            return parameters;
        }

        public virtual string BuildWhereClauseParameters(Object obj)
        {
            StringBuilder where = new StringBuilder().Append("WHERE ");
            Type type = obj.GetType();
            PropertyInfo[] props = type.GetProperties();
            string formatLike = "{0} LIKE '%{1}%' ";
            string formatFirst = "{0} = {1} ";

            if (props.Any())
            {
                int i = 0;
                foreach (PropertyInfo prop in props)
                {
                    if (prop.GetValue(obj, null) == null)
                        continue;

                    var format = i == 0 ? formatFirst : "AND " + formatFirst;

                    var val = prop.GetValue(obj, null);
                    switch (Type.GetTypeCode(prop.PropertyType))
                    {
                        case TypeCode.String:
                            where.Append(string.Format(i == 0 ? formatLike : "AND " + formatLike, prop.Name, val));
                            break;

                        case TypeCode.DateTime:
                            where.Append(string.Format(format, prop.Name, "'" + ((DateTime)val).ToShortDateString() + "'"));
                            break;

                        case TypeCode.Boolean:
                            where.Append(string.Format(format, prop.Name, (bool)val ? "1" : "0"));
                            break;

                        default:
                            where.Append(string.Format(format, prop.Name, val));
                            break;
                    }
                    i++;
                }

                return where.ToString();
            }
            else
                return string.Empty;
        }

        public PagedList<TView> FindAllWithPagination<TView, TViewSearchParam>(
            string sqlViewName, 
            string sortField, 
            TViewSearchParam search = null, 
            int page = 1,
            int rowPerPage = 10,
            bool orderAsc = true
        ) 
            where TView : class
            where TViewSearchParam : class, IViewSearchParam
        {
            if (page == 0) page = 1;
            string where = search == null || search.IsNull() ? 
                string.Empty : BuildWhereClauseParameters(search);

            string sql = $@"
                SELECT 
                * 
                FROM {sqlViewName}
                {@where}
                ORDER BY {sortField} {(orderAsc ? "ASC" : "DESC")}
                OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
            ";

            string sqlCount = $"SELECT COUNT(*) FROM {sqlViewName} {where}";

            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@PageSize", rowPerPage, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@PageNumber", page, DbType.Int32, ParameterDirection.Input);

            var res = CurrentConnection.Query<TView>(sql, parameter).ToList();
            var total = CurrentConnection.Query<int>(sqlCount, parameter).Single();
            return new PagedList<TView>(res, total, page, rowPerPage);
        }

        public PagedList<TView> FindAllWithPagination<TView, TFnSearchParam>(
            string sqlFunctionName, 
            string sortField, 
            IDictionary<int, string> param = null, 
            TFnSearchParam search = null, int page = 1,
            int rowPerPage = 10, 
            bool orderAsc = true
        )
            where TView : class
            where TFnSearchParam : class, IViewSearchParam
        {
            string where = search == null || search.IsNull() ? string.Empty : BuildWhereClauseParameters(search);

            var fnParam = param == null ||param.Count == 0 ? 
                string.Empty : string.Join(", ", param.OrderBy(p => p.Key).Select(p => p.Value).ToList());

            string sql = $@"
                SELECT 
                * 
                FROM {sqlFunctionName}({fnParam})
                {where}
                ORDER BY {sortField} {(orderAsc ? "ASC" : "DESC")}
                OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
            ";

            string sqlCount = $"SELECT COUNT(*) FROM {sqlFunctionName}({fnParam}) {@where}";

            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@PageSize", rowPerPage, DbType.Int32, ParameterDirection.Input);
            parameter.Add("@PageNumber", page, DbType.Int32, ParameterDirection.Input);

            var res = CurrentConnection.Query<TView>(sql, parameter).ToList();
            var total = CurrentConnection.Query<int>(sqlCount, parameter).Single();
            return new PagedList<TView>(res, total, page, rowPerPage);
        }

        #endregion
    }
}