﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Application.Etech.Core.Security
{
    /// <summary>
    /// Service hashage avec SHA256
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class SHA512CryptoService : IHashService
    {
        private static readonly Lazy<SHA512CryptoService> _instance = new Lazy<SHA512CryptoService>(() => (SHA512CryptoService)Activator.CreateInstance(typeof(SHA512CryptoService), true));

        private SHA512CryptoService() { }
        public static SHA512CryptoService Instance
        {
            get { return _instance.Value; }
        }

        /// <summary>
        /// Hash string with SHA256 crypto algorithm
        /// </summary>
        /// <param name="text"></param>
        /// <param name="salt">Salt : not used</param>
        /// <returns></returns>
        public string HashText(string text, string salt = null)
        {
            return string.Join(string.Empty,ComputeHash(text).Select(x => x.ToString("x2")));
        }

        public bool Verify(string text, string hashText)
        {
            var passToValidate = HashText(text);
            return passToValidate.Equals(hashText);
        }

        /// <summary>
        /// SHA256 crypto, ignore salt key
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static byte[] ComputeHash(string text)
        {
            using (var sha512 = new SHA512Managed())
            {
                byte[] byteValue = Encoding.UTF8.GetBytes(text);
                sha512.ComputeHash(byteValue);
                return sha512.Hash;
            }
        }
    }
}