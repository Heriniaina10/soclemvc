﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Application.Etech.Core.Security
{
    /// <summary>
    /// Service hashage avec SHA256
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class SHA256CryptoService : IHashService
    {
        private static readonly Lazy<SHA256CryptoService> _instance = new Lazy<SHA256CryptoService>(() => (SHA256CryptoService)Activator.CreateInstance(typeof(SHA256CryptoService), true));

        private SHA256CryptoService() { }
        public static SHA256CryptoService Instance
        {
            get { return _instance.Value; }
        }

        /// <summary>
        /// Hash string with SHA256 crypto algorithm
        /// </summary>
        /// <param name="text"></param>
        /// <param name="salt">Salt : not used</param>
        /// <returns></returns>
        public string HashText(string text, string salt = null)
        {
            //return Convert.ToBase64String(ComputeHash(text));
            return string.Join(string.Empty, ComputeHash(text).Select(x => x.ToString("x2")));
        }

        public bool Verify(string text, string hashText)
        {
            var passToValidate = HashText(text);
            return passToValidate.Equals(hashText);
        }

        /// <summary>
        /// SHA256 crypto, ignore salt key
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static byte[] ComputeHash(string text)
        {
            using (var sha256 = new SHA256CryptoServiceProvider())
            {
                byte[] byteValue = Encoding.UTF8.GetBytes(text);
                return sha256.ComputeHash(byteValue);
            }
        }
    }
}