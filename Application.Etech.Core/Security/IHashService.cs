﻿namespace Application.Etech.Core.Security
{
    public interface IHashService
    {
        string HashText(string text, string salt = null);

        bool Verify(string text, string hashText);
    }
}