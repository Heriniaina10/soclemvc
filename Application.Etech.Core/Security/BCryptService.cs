﻿using System;

namespace Application.Etech.Core.Security
{
    /// <summary>
    /// Service hashage avec BCrypt
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class BCryptService : IHashService
    {
        private static readonly Lazy<BCryptService> _instance = new Lazy<BCryptService>(() => (BCryptService)Activator.CreateInstance(typeof(BCryptService), true));

        private BCryptService() { }
        public static BCryptService Instance
        {
            get { return _instance.Value; }
        }
        public string HashText(string text, string salt = null)
        {
            return string.IsNullOrEmpty(salt)
                ? BCrypt.Net.BCrypt.HashPassword(text)
                : BCrypt.Net.BCrypt.HashPassword(text, salt);
        }

        public bool Verify(string text, string hashText)
        {
            return BCrypt.Net.BCrypt.Verify(text, hashText);
        }
    }
}