﻿using System;
using Microsoft.AspNet.Identity;

namespace Application.Etech.Core.Security.Identity
{
    /// <summary>
    /// Utiliser par <see cref="UserManager{TUser,TKey}"/>
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class BCryptPaswordHash : IPasswordHasher
    {
        private static readonly Lazy<BCryptPaswordHash> _instance = new Lazy<BCryptPaswordHash>(() => (BCryptPaswordHash)Activator.CreateInstance(typeof(BCryptPaswordHash), true));

        private BCryptPaswordHash() { }

        public static BCryptPaswordHash Instance
        {
            get { return _instance.Value; }
        }

        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(providedPassword, hashedPassword) ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;
        }
    }
}