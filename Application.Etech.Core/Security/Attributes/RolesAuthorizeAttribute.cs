﻿using System;
using System.Web.Http;

namespace Application.Etech.Core.Security.Attributes
{
    public class RolesAuthorizeAttribute : AuthorizeAttribute
    {
        public RolesAuthorizeAttribute(params string[] roles)
        {
            Roles = string.Join(",", roles);
        }
    }
}