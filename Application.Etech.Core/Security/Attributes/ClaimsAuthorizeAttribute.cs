﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http.Controllers;
using Application.Etech.Core.Models;
using Microsoft.AspNet.Identity;

namespace Application.Etech.Core.Security.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class ClaimsAuthorizeAttribute : RolesAuthorizeAttribute
    {
        private string _claimType;
        private IList<string> _claimValues;

        public ClaimsAuthorizeAttribute() { }
        public ClaimsAuthorizeAttribute(string type, params string[] values)
        {
            _claimType = type;
            _claimValues = values;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            
            var authorized = base.IsAuthorized(actionContext);
            var user = (ClaimsPrincipal)actionContext.RequestContext.Principal;
            var prop = actionContext.Request;

            if (authorized && !string.IsNullOrWhiteSpace(_claimType) && _claimValues?.Any()==true)
            {
                foreach (var claim in _claimValues)
                {
                    authorized = user.HasClaim(_claimType, claim);
                    if(!authorized) break;
                }
            }
            return authorized;
        }
    }
}