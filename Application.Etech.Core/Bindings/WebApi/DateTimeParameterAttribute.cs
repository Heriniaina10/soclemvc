﻿using System;
using System.ComponentModel;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Application.Etech.Core.Bindings.WebApi
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class DateTimeParameterAttribute : ParameterBindingAttribute
    {
        /// <summary>
        /// DateFormat : default value = dd/MM/yyyy
        /// </summary>
        [DefaultValue("dd/MM/yyyy")]
        public string DateFormat { get; set; }
        public bool ReadFromQueryString { get; set; }


        public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
        {
            if (parameter.ParameterType == typeof(DateTime)|| parameter.ParameterType == typeof(DateTime?))
            {
                var binding = new DateTimeParameterBinding(parameter)
                {
                    DateFormat = DateFormat,
                    ReadFromQueryString = ReadFromQueryString,
                    IsNullable = parameter.ParameterType == typeof(DateTime?)
                };
                return binding;
            }
            return parameter.BindAsError("Expected type DateTime?");
        }
    }
}