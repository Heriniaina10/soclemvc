﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;

namespace Application.Etech.Core.Bindings.WebApi
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class DateTimeParameterBinding : HttpParameterBinding
    {
        private static readonly string[] _customDateFormats = {
            "ddMMyyyyTHHmmssZ",
            "ddMMyyyyTHHmmZ",
            "ddMMyyyyTHHmmss",
            "ddMMyyyyTHHmm",
            "ddMMyyyyHHmmss",
            "ddMMyyyyHHmm",
            "ddMMyyyy",
            "dd/MM/yyyy HH:mm:ss",
            "dd/MM/yyyy",
            "dd-MM-yyyy HH-mm-ss",
            "dd-MM-yyyy HH-mm",
            "dd-MM-yyyy",
            "dd-MM-yyyy"
        };

        public string DateFormat { get; set; }
        [DefaultValue(true)]
        public bool IsNullable { get; set; }
        public bool ReadFromQueryString { get; set; }


        public DateTimeParameterBinding(HttpParameterDescriptor descriptor): base(descriptor) { }

        public override Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider,HttpActionContext actionContext,CancellationToken cancellationToken)
        {
            string dateToParse = null;
            var paramName = this.Descriptor.ParameterName;

            if (ReadFromQueryString)
            {
                // reading from query string
                var nameVal = actionContext.Request.GetQueryNameValuePairs();
                dateToParse = nameVal.Where(q => q.Key.Equals(paramName)).FirstOrDefault().Value;
            }
            else
            {
                // reading from route
                var routeData = actionContext.Request.GetRouteData();
                object dateObj = null;
                if (routeData.Values.TryGetValue(paramName, out dateObj))
                {
                    dateToParse = Convert.ToString(dateObj);
                }
            }

            DateTime? dateTime = null;
            if (!string.IsNullOrEmpty(dateToParse))
            {
                if (string.IsNullOrEmpty(DateFormat))
                {
                    dateTime = ParseDateTime(dateToParse);
                }
                else
                {
                    dateTime = ParseDateTime(dateToParse, new[] { DateFormat });
                }
            }

            SetValue(actionContext, IsNullable? dateTime : dateTime ?? DateTime.MinValue);

            return Task.FromResult<object>(null);
        }

        public DateTime? ParseDateTime(string dateToParse,string[] formats = null,IFormatProvider provider = null,DateTimeStyles styles = DateTimeStyles.AssumeLocal)
        {
            if (formats == null)
            {
                formats = _customDateFormats;
            }

            DateTime validDate;

            foreach (var format in formats)
            {
                if (format.EndsWith("Z"))
                {
                    if (DateTime.TryParseExact(dateToParse, format,provider,DateTimeStyles.AssumeUniversal,out validDate))
                    {
                        return validDate;
                    }
                }
                else
                {
                    if (DateTime.TryParseExact(dateToParse, format, provider, styles, out validDate))
                    {
                        return validDate;
                    }
                }
            }

            return null;
        }
    }
}