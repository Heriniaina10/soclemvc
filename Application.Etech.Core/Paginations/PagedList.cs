﻿using System.Collections.Generic;

namespace Application.Etech.Core.Paginations
{
    /// <summary>
    /// Modèle pour la pagination côté service
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    /// <author>Fenonantenain</author>
    public sealed class PagedList<TModel> where TModel : class
    {
        public IEnumerable<TModel> Items { get; protected set; }

        public int TotalRowCount { get; protected set; }

        public int CurrentPage { get; set; }

        public int RowPerPage { get; set; }

        public bool HasNext
        {
            get { return TotalRowCount > (CurrentPage * RowPerPage); }
        }

        public PagedList(IEnumerable<TModel> source = null, int total = 0, int page = 1, int row = 10)
        {
            Items = source;
            TotalRowCount = total;
            CurrentPage = page;
            RowPerPage = row;
        }

    }
}