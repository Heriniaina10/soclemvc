﻿using System;
using System.Configuration;
using Application.Etech.Core.Configurations.Elements;

namespace Application.Etech.Core.Configurations.Sections
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class JwtSecuritySection : ConfigurationSection
    {
        [ConfigurationProperty("refreshToken")]
        public ItemSetting<bool> EnableRefreshToken => (ItemSetting<bool>)this["refreshToken"];

        [ConfigurationProperty("signatureKey", IsRequired = true)]
        public ItemSetting<string> SignatureKey => (ItemSetting<string>)this["signatureKey"];

        [ConfigurationProperty("issuers")]
        public ItemSetting<string> Issuer => (ItemSetting<string>) this["issuers"];

        [ConfigurationProperty("audiences")]
        public ItemSetting<string> Audience => (ItemSetting<string>)this["audiences"];

        [ConfigurationProperty("expiration", IsRequired = true)]
        public ExpirationSetting Expiration => (ExpirationSetting) this["expiration"];

        [ConfigurationProperty("algorithm", IsRequired = true)]
        public AlgorithmSetting Algorithm => (AlgorithmSetting)this["algorithm"];

        private static readonly Lazy<JwtSecuritySection> _instance = new Lazy<JwtSecuritySection>(() => (JwtSecuritySection)ConfigurationManager.GetSection("jwtSecurity") ?? new JwtSecuritySection());
        public static JwtSecuritySection Instance => _instance.Value;
    }
}