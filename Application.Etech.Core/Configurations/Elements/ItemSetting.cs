﻿using System;
using System.Configuration;

namespace Application.Etech.Core.Configurations.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <author>Fenonantenaina</author>
    public class ItemSetting<T> : ConfigurationElement
    {
        private ItemSetting()
        {
            Value = default(T);
        }

        [ConfigurationProperty("value")]
        public T Value
        {
            get => (T)this["value"];
            set => this["value"] = value;
        }
    }
}