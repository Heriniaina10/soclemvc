﻿using System.Configuration;

namespace Application.Etech.Core.Configurations.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class ExpirationSetting : ConfigurationElement
    {
        private ExpirationSetting() { }

        [ConfigurationProperty("token")]
        public ItemSetting<int> TokenLifeTime => (ItemSetting<int>) this["token"];

        [ConfigurationProperty("refreshToken")]
        public ItemSetting<int> RefreshTokenLifeTime => (ItemSetting<int>)this["refreshToken"];
    }
}