﻿using System.Configuration;

namespace Application.Etech.Core.Configurations.Elements
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class AlgorithmSetting : ConfigurationElement
    {
        private AlgorithmSetting() { }

        [ConfigurationProperty("signature")]
        public ItemSetting<string> Signature => (ItemSetting<string>) this["signature"];

        [ConfigurationProperty("digest")]
        public ItemSetting<string> Digest => (ItemSetting<string>)this["digest"];
    }
}