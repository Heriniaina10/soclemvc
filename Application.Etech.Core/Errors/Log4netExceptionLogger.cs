﻿using System.Web.Http.ExceptionHandling;

namespace Application.Etech.Core.Errors
{
    public class Log4netExceptionLogger : System.Web.Http.ExceptionHandling.ExceptionLogger
    {
        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(Log4netExceptionLogger));
        public override void Log(ExceptionLoggerContext context)
        {
            var message = context.Exception.Message;
            Logger.Error(message, context.Exception);
        }
    }
}