﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace Application.Etech.Core.Errors
{
    public static class ModelStateExtensions
    {
        public static IEnumerable<ErrorItem> AllErrors(this ModelStateDictionary modelState)
        {
            var result = from ms in modelState
                         where ms.Value.Errors.Any()
                         let fieldKey = ms.Key
                         let errors = ms.Value.Errors
                         from error in errors
                         select new ErrorItem(fieldKey, error.ErrorMessage);

            return result;
        }
        public static IEnumerable<string> AllErrorsString(this ModelStateDictionary modelState)
        {
            var result = from ms in modelState
                         where ms.Value.Errors.Any()
                         let fieldKey = ms.Key
                         let errors = ms.Value.Errors
                         from error in errors
                         select new ErrorItem(fieldKey, error.ErrorMessage).ToString();

            return result;
        }
    }
}