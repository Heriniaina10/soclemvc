﻿using System.Net;

namespace Application.Etech.Core.Errors.Exceptions
{
    public class TechnicalException : ApplicationException
    {
        public TechnicalException(string errorMessage, HttpStatusCode code = HttpStatusCode.InternalServerError) : base(errorMessage, code)
        {
        }
    }
}