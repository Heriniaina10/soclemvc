﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http.ModelBinding;

namespace Application.Etech.Core.Errors.Exceptions
{
    public class FunctionalException : ApplicationException
    {
        public ModelStateDictionary ModelState { get; private set; }
        public Dictionary<string,IList<string>> ModelStateApi { get; private set; } = new Dictionary<string, IList<string>>();
        public FunctionalException(string errorMessage, ModelStateDictionary modelState=null, HttpStatusCode code = HttpStatusCode.BadRequest) : base(errorMessage, code)
        {
            ModelState = modelState;
            LoadApiMessage();
        }

        private void LoadApiMessage()
        {
            if (ModelState == null) return;
            foreach (var key in ModelState.Keys)
            {
                ModelStateApi.Add(key, ModelState[key].Errors.Select(x=>x.ErrorMessage).ToList());
            }
        }
    }
}