﻿using System;
using System.Net;

namespace Application.Etech.Core.Errors.Exceptions
{
    public class ApplicationException : Exception
    {
        public string ErrorMessage { get; protected set; }
        public HttpStatusCode Status { get; set; }

        public ApplicationException(string errorMessage, HttpStatusCode code) : base(errorMessage)
        {
            this.ErrorMessage = errorMessage;
            this.Status = code;
        }
    }
}