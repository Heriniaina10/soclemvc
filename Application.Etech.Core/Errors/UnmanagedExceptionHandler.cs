﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using Application.Etech.Core.Errors.Exceptions;

namespace Application.Etech.Core.Errors
{
    public class UnmanagedExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            base.Handle(context);
        }

        public async override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            var exception = context.Exception;
            var response = context.Request.CreateResponse(HttpStatusCode.InternalServerError,
                new
                {
                    Status = HttpStatusCode.InternalServerError,
                    Message = (exception.InnerException ?? exception).Message,
                    Reason = "Internal Server Error.Please Contact your Administrator."
                });

            context.Result = new ResponseMessageResult(response);
        }
    }
}