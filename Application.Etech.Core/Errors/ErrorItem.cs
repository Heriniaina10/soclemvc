﻿namespace Application.Etech.Core.Errors
{
    /// <summary>
    /// Erreur Api
    /// </summary>
    /// <author>Fenonantenaina</author>
    public class ErrorItem
    {
        public ErrorItem(string key, string message)
        {
            Key = key;
            Message = message;
        }

        public string Key { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return $"{Key}:{Message}";
        }
    }
}