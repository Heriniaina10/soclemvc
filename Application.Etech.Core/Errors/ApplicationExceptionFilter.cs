﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using Application.Etech.Core.Errors.Exceptions;

namespace Application.Etech.Core.Errors
{
    /// <summary>
    /// <exception cref="ApplicationException">ApplicationException</exception> Filter
    /// </summary>
    public class  ApplicationExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            string exceptionMessage = string.Empty;
            if (actionExecutedContext.Exception is ApplicationException exception)
            {
                HttpResponseMessage response = null;
                if (exception is TechnicalException technicalException)
                {
                    response = actionExecutedContext.Request.CreateResponse(technicalException.Status,
                        new
                        {
                            technicalException.Status,
                            Message = technicalException.ErrorMessage,
                            Reason = "Internal Server Error.Please Contact your Administrator."
                        });
                }
                else if (exception is FunctionalException functionalException)
                {
                    response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.OK,
                        new {
                            functionalException.Status,
                            Message = functionalException.ErrorMessage,
                            Reason = "Functional Error.Please verify your data.",
                            ModelState = functionalException.ModelStateApi
                        });
                }
                else
                {
                    response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.InternalServerError,
                        new
                        {
                            exception.Status,
                            Message = new StringContent(exception.ErrorMessage),
                            Reason = "Application error."
                        });
                }
                actionExecutedContext.Response = response;
            }

        }
    }
}