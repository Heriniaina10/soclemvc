﻿using Application.Etech.Core.Enums;
using Application.Etech.Core.Errors.Exceptions;
using Dapper.Contrib.Extensions;
using Mehdime.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Application.Etech.Core.Extensions
{
    public static class EntityAuditExtension
    {
        public static int SaveChangeWithAudit<TEntityAudit, TContext>(this IDbContextScope dbContext) 
            where TEntityAudit : class, new()
            where TContext : DbContext
        {
            int res = 0;
            var context = dbContext.DbContexts.Get<TContext>();
            context.ChangeTracker.DetectChanges();

            List<TEntityAudit> audits = new List<TEntityAudit>();

            var added = context.ChangeTracker.Entries()
                .Where(t => t.State == EntityState.Added)
                .ToArray();

            #region Modification
            var edited = context.ChangeTracker.Entries()
            .Where(t => t.State == EntityState.Modified)
            .ToArray();       

            foreach (var entry in edited)
            {
                if (!IsSameType<TEntityAudit>(entry))
                    continue;

                dynamic entityAuditBefore = new TEntityAudit();
                entityAuditBefore.Action = EntityUpdateActionTypes.BeforeUpdate;
                foreach (var prop in entry.OriginalValues.PropertyNames.Where(pn => pn != "Date" && pn != "Action" && pn != "UserName" && pn != "IsDeleted"))
                {
                    entityAuditBefore.GetType().GetProperty(prop).SetValue(entityAuditBefore, entry.OriginalValues[prop], null);
                }

                dynamic entityAuditAfter = new TEntityAudit();
                entityAuditAfter.Action = EntityUpdateActionTypes.AfterUpdate;
                foreach (var prop in entry.CurrentValues.PropertyNames.Where(pn => pn != "Date" && pn != "Action" && pn != "UserName" && pn != "IsDeleted"))
                {
                    entityAuditAfter.GetType().GetProperty(prop).SetValue(entityAuditAfter, entry.CurrentValues[prop], null);
                }

                entityAuditAfter.Date = entityAuditBefore.Date;

                audits.AddRange(new List<TEntityAudit> { entityAuditBefore, entityAuditAfter });
            }
            #endregion

            #region Delete

            var deleted = context.ChangeTracker.Entries()
                .Where(t => t.State == EntityState.Deleted)
                .ToArray();

            foreach (var entry in deleted)
            {
                if (!IsSameType<TEntityAudit>(entry))
                    continue;

                dynamic entityAudit = new TEntityAudit();

                entityAudit.Action = EntityUpdateActionTypes.Deleted;

                foreach (var prop in entry.OriginalValues.PropertyNames.Where(pn => pn != "Date" && pn != "Action" && pn != "UserName" && pn != "IsDeleted"))
                {
                    entityAudit.GetType().GetProperty(prop).SetValue(entityAudit, entry.OriginalValues[prop], null);
                }

                audits.Add(entityAudit);
            }
            #endregion

            try
            {
                res = dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new FunctionalException(e.InnerException == null ? e.Message : e.InnerException.InnerException.Message);
            }

            #region Ajout
            foreach (var entry in added)
            {
                if (!IsSameType<TEntityAudit>(entry))
                    continue;

                dynamic entityAudit = new TEntityAudit();
                entityAudit.Action = EntityUpdateActionTypes.Added;
                
                foreach (var prop in entry.CurrentValues.PropertyNames.Where(pn => pn != "Date" && pn != "Action" && pn != "UserName" && pn != "IsDeleted"))
                {
                    entityAudit.GetType().GetProperty(prop).SetValue(entityAudit, entry.CurrentValues[prop], null);
                }

                audits.Add(entityAudit);
            }
            #endregion

            context.Database.Connection.Insert(audits);
            return res;
        }

        private static bool IsSameType<TEntityAudit>(DbEntityEntry entry)
        {
            Type entityType = entry.Entity.GetType();
            return entityType.Name.Split('_')[0] == typeof(TEntityAudit).Name.Replace("Logs", "");
        }
    }
}
