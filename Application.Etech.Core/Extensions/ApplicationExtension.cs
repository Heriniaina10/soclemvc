﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Application.Etech.Core.Extensions
{
    public static class ApplicationExtension
    {
        public static IOrderedQueryable<TEntity> CustomOrderBy<TEntity>(this IQueryable<TEntity> source, String propertyName, bool isAsc=true) where TEntity : class
        {
            // Create a parameter to pass into the Lambda expression (Entity => Entity.OrderByField).
            var parameter = Expression.Parameter(typeof(TEntity), "Entity");
            //  create the selector part, but support child properties
            PropertyInfo property;
            Expression propertyAccess;
            if (propertyName.Contains("."))
            {
                // support to be sorted on child fields.
                var childProperties = propertyName.Split('.');
                property = typeof(TEntity).GetProperty(childProperties[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
                for (int i = 1; i < childProperties.Length; i++)
                {
                    property = property.PropertyType.GetProperty(childProperties[i], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    propertyAccess = Expression.MakeMemberAccess(propertyAccess, property);
                }
            }
            else
            {
                property = typeof(TEntity).GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
            }

            // Create the order by expression.
            var lambaExpression = Expression.Lambda(propertyAccess, parameter);
            var methodName = isAsc ? "OrderBy" : "OrderByDescending";
            var resultExp = Expression.Call(typeof(Queryable), methodName,
                            new [] { typeof(TEntity), property.PropertyType },
                            source.Expression, Expression.Quote(lambaExpression));
            return source.Provider.CreateQuery<TEntity>(resultExp) as IOrderedQueryable<TEntity>;
        }

        public static bool In<T>(this T source, params T[] list)
        {
            return (list as IList<T>).Contains(source);
        }

        /// <summary>
        /// Arranger la liste en une liste supporté par Mobile.
        /// <para>
        /// Exemple : 
        /// La liste <code>[1,2,3,4,5,6,7,8,9]</code> devient <code>[1,6,2,7,3,8,4,9,5]</code>
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static List<T> ArrangeAsMobileList<T>(this List<T> source)
        {
            if(source == null) return new List<T>();
            var milieu = (int)Math.Ceiling(source.Count / 2.0);
            var result = new List<T>(source.Count);
            for (var i = 0; i < milieu; i++)
            {
                result.Add(source[i]);
                var idxRight = milieu + i;
                if (idxRight < source.Count)
                {
                    result.Add(source[idxRight]);
                }
            }
            return result;
        }

        /// <summary>
        /// Recuperer la date du premier jour de la semaine
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="startOfWeek"></param>
        /// <returns></returns>
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
        
         public static String RemoveDiacritics(this String s)
         {
                String normalizedString = s.Normalize(NormalizationForm.FormD);
                StringBuilder stringBuilder = new StringBuilder();

                for (int i = 0; i < normalizedString.Length; i++)
                {
                    Char c = normalizedString[i];
                    if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    {
                        stringBuilder.Append(c);
                    }
                }

                return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
            }
        
    }
}