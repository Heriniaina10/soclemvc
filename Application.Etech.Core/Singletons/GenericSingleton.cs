﻿using System;

namespace Application.Etech.Core.Singletons
{
    /// <summary>
    /// Singleton pattern
    /// </summary>
    /// <typeparam name="TSingleton"></typeparam>
    public abstract class GenericSingleton<TSingleton> where TSingleton:GenericSingleton<TSingleton>
    {
        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(TSingleton));
        private static readonly Lazy<TSingleton> _instance = new Lazy<TSingleton>(() => (TSingleton)Activator.CreateInstance(typeof(TSingleton), true));

        protected GenericSingleton() { }

        public static TSingleton Instance
        {
            get
            {
                return _instance.Value;
            }
        }
    }
}