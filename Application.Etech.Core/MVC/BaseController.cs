﻿using System.Web.Mvc;
using Mehdime.Entity;

namespace Application.Etech.Core.MVC
{
    /// <summary>
    /// Base controller
    /// </summary>
    /// <author>Fenonantenainae</author>
    public abstract class BaseController : Controller
    {
        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected readonly IDbContextScopeFactory ContextScopeFactory;

        protected BaseController()
        {
            ContextScopeFactory = new DbContextScopeFactory();
        }

    }
}