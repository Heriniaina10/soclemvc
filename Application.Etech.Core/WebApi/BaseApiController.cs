﻿using System.Web.Http;
using Mehdime.Entity;

namespace Application.Etech.Core.WebApi
{
    /// <summary>
    /// WebApi base controller
    /// </summary>
    /// <author>Fenonantenainae</author>
    public abstract class BaseApiController : ApiController
    {
        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected readonly IDbContextScopeFactory ContextScopeFactory;

        protected BaseApiController()
        {
            ContextScopeFactory = new DbContextScopeFactory();
        }

    }
}