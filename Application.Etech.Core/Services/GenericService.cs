﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using Application.Etech.Core.Extensions;
using System.Web.Mvc;
using Application.Etech.Core.Models;
using Application.Etech.Core.Paginations;
using Application.Etech.Core.Repository;
using Mehdime.Entity;

namespace Application.Etech.Core.Services
{
    /// <summary>
    /// Service générique
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TRepository"></typeparam>
    /// <typeparam name="TService"></typeparam>
    /// <author>Fenonantenaina</author>
    public abstract class GenericService<TEntity, TContext, TRepository, TService> : IService<TEntity>
        where TEntity : class
        where TContext : DbContext
        where TRepository : IRepository<TEntity>
        where TService : IService<TEntity>
    {

        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(TService));
        private static readonly Lazy<TService> _instance = new Lazy<TService>(() => (TService)Activator.CreateInstance(typeof(TService), true));
        protected readonly IDbContextScopeFactory ContextScopeFactory;

        protected GenericService()
        {
            ContextScopeFactory = new DbContextScopeFactory();
            Repository = GenericRepository<TEntity, TContext, TRepository>.Instance;
           
        }

        protected TRepository Repository { get; set; }

        /// <summary>
        /// Recuperer l'instance singleton
        /// </summary>
        public static TService Instance => _instance.Value;

        #region MAJ

        public TEntity New()
        {
            using (ContextScopeFactory.Create())
            {
                return Repository.New();
            }
        }

        public virtual void Add(params TEntity[] entity)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                Repository.Add(entity);
                dbContext.SaveChanges();
            }
        }

        public virtual void Attach(TEntity entity)
        {
            using (ContextScopeFactory.Create())
            {
                Repository.Attach(entity);
            }
        }

        public virtual void Update(TEntity entity)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                Repository.Update(entity);
                dbContext.SaveChanges();
            }
        }

        public virtual void Delete(TEntity entity)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                Repository.Delete(entity);
                dbContext.SaveChanges();
            }
        }

        public virtual void DeleteById(object id)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                Repository.DeleteById(id);
                dbContext.SaveChanges();
            }
        }


        /// <summary>
        /// Supprime tous les enregistrements d'une table
        /// </summary>
        public virtual  void DeleteAll()
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                Repository.DeleteAll();
                dbContext.SaveChanges();
            }
        }

        #endregion

        #region RETRIEVE ALL

        public virtual IList<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Repository.FindAll(predicate).ToList();
            }
        }

        public virtual IList<TModel> FindAll<TModel>(Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Repository.FindAll(selector,predicate).ToList();
            }
        }

        public virtual PagedList<TEntity> FindAllWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowPerPage = 10, bool orderAsc = true)
        {
            
                using (ContextScopeFactory.CreateReadOnly())
                {
                    var total = Repository.Count(predicate);
                    var items = Repository.QueryWithPagination(sortField, predicate, page, rowPerPage, orderAsc).ToList();
                    return new PagedList<TEntity>(items, total, page, rowPerPage);
                }
            
          
        }

        public virtual PagedList<TModel> FindAllWithPagination<TModel>(string sortField, Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowPerPage = 10, bool orderAsc = true) where TModel : class
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var total = Repository.Count(predicate);
                var items = Repository.QuerySelectorWithPagination(sortField, selector, predicate, page, rowPerPage, orderAsc).ToList();
                return new PagedList<TModel>(items, total, page, rowPerPage);
            }
        }

        #endregion

        #region FIND

        public virtual TEntity Find(object id)
        {
            using (ContextScopeFactory.Create())
            {
                return Repository.Find(id);
            }
        }

        public virtual TEntity First(Expression<Func<TEntity, bool>> predicate)
        {
            using (ContextScopeFactory.Create())
            {
                return Repository.First(predicate);
            }
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            using (ContextScopeFactory.Create())
            {
                return Repository.FirstOrDefault(predicate);
            }
        }

        public virtual int Count(Expression<Func<TEntity, bool>> predicate = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Repository.Count(predicate);
            }
        }

        #endregion


        #region DROPDOWN

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <typeparam name="TValue">Type valeur</typeparam>
        /// <typeparam name="TLabel">Type display text</typeparam>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="defaultOption"></param>
        /// <returns></returns>
        public virtual IList<LabelValue<TValue, TLabel>> GetLabelValueList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null, LabelValue<TValue, TLabel> defaultOption = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var items = Repository.QuerySelector(selector, predicate).ToList();
                if (defaultOption != null) items.Insert(0, defaultOption);
                return items;
            }
        }

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <typeparam name="TValue">Type valeur</typeparam>
        /// <typeparam name="TLabel">Type display text</typeparam>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="selectedValue">Selected object value</param>
        /// <param name="defaultOption">Valeur par défaut</param>
        /// <returns></returns>
        public virtual SelectList GetSelectList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null, LabelValue<TValue, TLabel> defaultOption = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var items = Repository.QuerySelector(selector, predicate).ToList();
                if (defaultOption != null) items.Insert(0, defaultOption);
                return new SelectList(items, "Value", "Label", selectedValue);
            }
        }

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="selectedValue">Selected object value</param>
        /// <param name="defaultOption">Valeur par défaut</param>
        /// <returns></returns>
        public virtual SelectList GetSelectList(Expression<Func<TEntity, SelectListItem>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null, SelectListItem defaultOption = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var items = Repository.QuerySelector(selector, predicate).ToList();
                if (defaultOption != null) items.Insert(0, defaultOption);
                return new SelectList(items, "Value", "Text", selectedValue);
            }
        }

        #endregion

        #region SQLQUERY

        public virtual IList<TEntity> SqlQueryToList(string sqlOrProcedure, params SqlParameter[] args)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Repository.SqlQuery<TEntity>(sqlOrProcedure, args).ToList();
            }
        }

        public virtual IList<TModel> SqlQueryToList<TModel>(string sqlOrProcedure, params SqlParameter[] args)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Repository.SqlQuery<TModel>(sqlOrProcedure, args).ToList();
            }
        }

        public virtual TEntity SqlQueryToObject(string sqlOrProcedure, params SqlParameter[] args)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Repository.SqlQuery<TEntity>(sqlOrProcedure, args).FirstOrDefault();
            }
        }

        public virtual TModel SqlQueryToObject<TModel>(string sqlOrProcedure, params SqlParameter[] args)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Repository.SqlQuery<TModel>(sqlOrProcedure, args).FirstOrDefault();
            }
        }

        public virtual void ExecuteNoQuery(string sqlOrProcedure, params SqlParameter[] args)
        {
            using (ContextScopeFactory.Create())
            {
                Repository.ExecuteNoQuery(sqlOrProcedure, args);
            }
        }

        #endregion

    }
}