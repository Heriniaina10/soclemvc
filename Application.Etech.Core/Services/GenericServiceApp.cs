﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Application.Etech.Core.Extensions;
using Application.Etech.Core.Models;
using Application.Etech.Core.Paginations;
using Application.Etech.Core.Repository;
using Mapster;
using Mehdime.Entity;

namespace Application.Etech.Core.Services
{
    /// <summary>
    /// Generic SA
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TDto"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    /// <typeparam name="TRepository"></typeparam>
    /// <typeparam name="TService"></typeparam>
    /// <typeparam name="TServiceApp"></typeparam>
    /// <author>Fenonantenaina</author>
    public class GenericServiceApp<TEntity, TDto, TContext, TRepository, TService, TServiceApp> : IServiceApp<TEntity, TDto> 
        where TEntity : class
        where TDto : class
        where TContext : DbContext
        where TRepository : IRepository<TEntity>
        where TService : IService<TEntity>
        where TServiceApp : IServiceApp<TEntity,TDto>
    {

        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(typeof(TServiceApp));
        private static readonly Lazy<TServiceApp> _instance = new Lazy<TServiceApp>(() => (TServiceApp)Activator.CreateInstance(typeof(TServiceApp), true));
        protected readonly IDbContextScopeFactory ContextScopeFactory;

        protected GenericServiceApp()
        {
            ContextScopeFactory = new DbContextScopeFactory();
            Service = GenericService<TEntity, TContext, TRepository, TService>.Instance;
        }

        protected TService Service { get; set; }

        /// <summary>
        /// Recuperer l'instance singleton
        /// </summary>
        public static TServiceApp Instance => _instance.Value;

        public virtual void Add(params TDto[] dto)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                var entity = dto.Adapt<IEnumerable<TDto>, IList<TEntity>>()?.ToArray();
                Service.Add(entity);
                dbContext.SaveChanges();
                //dbContext.SaveChangeWithAudit<TEntityAudit, TContext>();
            }
        }
        
        public virtual async Task AddAsync(TDto dto)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                var entity = dto.Adapt<TDto, TEntity>();
                Service.Add(entity);
                await dbContext.SaveChangesAsync();
            }
        }

        public virtual void Update(TDto dto)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                var entity = dto.BuildAdapter()
                    .CreateEntityFromContext(dbContext.DbContexts.Get<TContext>())
                    .AdaptToType<TEntity>();
                Service.Update(entity);
                dbContext.SaveChanges();

                //dbContext.SaveChangeWithAudit<TEntityAudit, TContext>();
            }
        }
        
        public virtual async Task UpdateAsync(TDto dto)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                var entity = dto.BuildAdapter()
                    .CreateEntityFromContext(dbContext.DbContexts.Get<TContext>())
                    .AdaptToType<TEntity>();
                Service.Update(entity);
                await dbContext.SaveChangesAsync();
            }
        }

        public virtual void Delete(TDto dto)
        {
            using (var dbContext = ContextScopeFactory.Create())
            {
                Service.Delete(dto.Adapt<TDto, TEntity>());
                dbContext.SaveChanges();

                //dbContext.SaveChangeWithAudit<TEntityAudit, TContext>();
            }
        }

        public virtual void DeleteById(object id)
        {
            using (var context = ContextScopeFactory.Create())
            {
                Service.DeleteById(id);
                context.SaveChanges();

                //context.SaveChangeWithAudit<TEntityAudit, TContext>();
            }
        }

        public virtual IList<TDto> FindAll(Expression<Func<TEntity, bool>> predicate = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var entities = Service.FindAll(predicate);
                return entities.Adapt<IEnumerable<TEntity> ,IList <TDto>>();
            }
        }

        public virtual IList<LabelValue<TValue, TLabel>> GetLabelValueList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null,
            LabelValue<TValue, TLabel> defaultOption = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Service.GetLabelValueList<TValue, TLabel>(selector, predicate, defaultOption);
            }
        }

        public virtual SelectList GetSelectList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null,
            LabelValue<TValue, TLabel> defaultOption = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Service.GetSelectList<TValue, TLabel>(selector, predicate, defaultOption);
            }
        }

        public virtual SelectList GetSelectList(Expression<Func<TEntity, SelectListItem>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null,
            SelectListItem defaultOption = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Service.GetSelectList(selector, predicate, defaultOption);
            }
        }

        public virtual IList<TInfoDto> FindAll<TInfoDto>(Expression<Func<TEntity, bool>> predicate = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var entities = Service.FindAll(predicate);
                return entities.Adapt<IList<TEntity>, IList<TInfoDto>>();
            }
        }

        public virtual PagedList<TInfoDto> FindAllWithPagination<TInfoDto>(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1,
            int rowPerPage = 10, bool orderAsc = true) where TInfoDto : class
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var paged = Service.FindAllWithPagination(sortField, predicate, page, rowPerPage, orderAsc);
                return new PagedList<TInfoDto>(paged.Items.Adapt<IEnumerable<TEntity>, IList<TInfoDto>>(), paged.TotalRowCount, paged.CurrentPage, paged.RowPerPage);
            }
        }

        public virtual PagedList<TDto> FindAllWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null,
            int page = 1, int rowPerPage = 10, bool orderAsc = true)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                var paged = Service.FindAllWithPagination(sortField, predicate, page, rowPerPage, orderAsc);
                return new PagedList<TDto>(paged.Items.Adapt<IEnumerable<TEntity>, IList<TDto>>(), paged.TotalRowCount, paged.CurrentPage, paged.RowPerPage);
            }
        }

        public virtual TDto Find(object id)
        {
            using (ContextScopeFactory.Create())
            {
                return Service.Find(id).Adapt<TDto>();
            }
        }

        public virtual int Count(Expression<Func<TEntity, bool>> predicate = null)
        {
            using (ContextScopeFactory.CreateReadOnly())
            {
                return Service.Count(predicate);
            }
        }
    }
}