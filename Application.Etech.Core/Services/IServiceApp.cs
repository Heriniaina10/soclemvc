﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Application.Etech.Core.Models;
using Application.Etech.Core.Paginations;

namespace Application.Etech.Core.Services
{
    public interface IServiceApp<TEntity,TDto> where TEntity : class where TDto : class
    {
        /// <summary>
        /// Ajouter un enregistrement dans la base
        /// </summary>
        /// <param name="dto"></param>
        void Add(params TDto[] dto);
        
        /// <summary>
        /// Ajouter un enregistrement dans la base
        /// </summary>
        /// <param name="dto"></param>
        Task AddAsync(TDto dto);

        /// <summary>
        /// Update un enregistrement dans la base
        /// </summary>
        /// <param name="dto"></param>
        void Update(TDto dto);
        
        /// <summary>
        /// Update un enregistrement dans la base
        /// </summary>
        /// <param name="dto"></param>
        Task UpdateAsync(TDto dto);

        /// <summary>
        /// Supprimer un enregistrement
        /// </summary>
        /// <param name="dto"></param>
        void Delete(TDto dto);

        /// <summary>
        /// Supprimer un enregistrement à partir son id
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(object id);

        /// <summary>
        /// Récupérer tous les enregistrements selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IList<TDto> FindAll(Expression<Func<TEntity, bool>> predicate = null);


        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <typeparam name="TValue">Type valeur</typeparam>
        /// <typeparam name="TLabel">Type display text</typeparam>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="defaultOption"></param>
        /// <returns></returns>
        IList<LabelValue<TValue, TLabel>> GetLabelValueList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null, LabelValue<TValue, TLabel> defaultOption = null);

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <typeparam name="TValue">Type valeur</typeparam>
        /// <typeparam name="TLabel">Type display text</typeparam>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="selectedValue">Selected object value</param>
        /// <param name="defaultOption">Valeur par défaut</param>
        /// <returns></returns>
        SelectList GetSelectList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null, LabelValue<TValue, TLabel> defaultOption = null);

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="selectedValue">Selected object value</param>
        /// <param name="defaultOption">Valeur par défaut</param>
        /// <returns></returns>
        SelectList GetSelectList(Expression<Func<TEntity, SelectListItem>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null, SelectListItem defaultOption = null);

        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        PagedList<TDto> FindAllWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowPerPage = 10, bool orderAsc = true);

        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        PagedList<TInfoDto> FindAllWithPagination<TInfoDto>(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowPerPage = 10, bool orderAsc = true) where TInfoDto : class;

        /// <summary>
        /// Récupère un enregistrement depuis la base par son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TDto Find(object id);

        /// <summary>
        /// Retourne le nombre d'enregistrement correspond au critère [predicate].
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        int Count(Expression<Func<TEntity, bool>> predicate = null);

    }
}