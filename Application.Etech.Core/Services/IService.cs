﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Web.Mvc;
using Application.Etech.Core.Models;
using Application.Etech.Core.Paginations;

namespace Application.Etech.Core.Services
{
    public interface IService<TEntity> where TEntity : class 
    {

        /// <summary>
        /// Créer une nouvelle instance de Entité rattachée au context courant 
        /// </summary>
        TEntity New();
        /// <summary>
        /// Ajouter un enregistrement dans la base
        /// </summary>
        /// <param name="entity"></param>
        void Add(params TEntity[] entity);
        /// <summary>
        /// Attacher un objet détaché dans le context courant
        /// </summary>
        /// <param name="entity"></param>
        void Attach(TEntity entity);
        /// <summary>
        /// Update un enregistrement dans la base
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);
        /// <summary>
        /// Supprimer un enregistrement
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// Supprimer un enregistrement à partir son id
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(object id);
        /// <summary>
        /// Récupérer tous les enregistrements selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IList<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate = null);
        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur selon le critère [predicate] (si non null).
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IList<TModel> FindAll<TModel>(Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null);
        /// <summary>
        /// Récupérer tous les enregistrements avec une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        PagedList<TEntity> FindAllWithPagination(string sortField, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowPerPage = 10, bool orderAsc = true);
        /// <summary>
        /// Récupérer tous les enregistrements avec un selecteur et une pagination selon le critère [predicate] (si non null).
        /// <para>Pour la pagination, les enregistrements doivent être classé par ordre croissante ou décroissante</para>
        /// <para>Les enregistrements seront chargé en mémoire</para>
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="sortField"></param>
        /// <param name="selector"></param>
        /// <param name="predicate"></param>
        /// <param name="page"></param>
        /// <param name="rowPerPage"></param>
        /// <param name="orderAsc"></param>
        /// <returns></returns>
        PagedList<TModel> FindAllWithPagination<TModel>(string sortField, Expression<Func<TEntity, TModel>> selector, Expression<Func<TEntity, bool>> predicate = null, int page = 1, int rowPerPage = 10, bool orderAsc = true) where TModel : class;

        /// <summary>
        /// Récupère un enregistrement depuis la base par son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity Find(object id);
        /// <summary>
        /// Récupère le premier enregistrement qui satisfait le critère [predicate]
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Null si aucun enregistrement trouvé</returns>
        TEntity First(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Récupère le premier enregistrement qui satisfait le critère [predicate]
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>La valeur default() si aucun enregistrement trouvé</returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Retourne le nombre d'enregistrement correspond au critère [predicate].
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        int Count(Expression<Func<TEntity, bool>> predicate = null);

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <typeparam name="TValue">Type valeur</typeparam>
        /// <typeparam name="TLabel">Type display text</typeparam>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="defaultOption"></param>
        /// <returns></returns>
        IList<LabelValue<TValue, TLabel>> GetLabelValueList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null, LabelValue<TValue, TLabel> defaultOption = null);

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <typeparam name="TValue">Type valeur</typeparam>
        /// <typeparam name="TLabel">Type display text</typeparam>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="selectedValue">Selected object value</param>
        /// <param name="defaultOption">Valeur par défaut</param>
        /// <returns></returns>
        SelectList GetSelectList<TValue, TLabel>(Expression<Func<TEntity, LabelValue<TValue, TLabel>>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null, LabelValue<TValue, TLabel> defaultOption = null);

        /// <summary>
        /// Récuperer la liste d'objet LabelValue pour dropdown ou autre
        /// </summary>
        /// <param name="selector">Sélécteur pour former l'objet LabelValue</param>
        /// <param name="predicate">Critère</param>
        /// <param name="selectedValue">Selected object value</param>
        /// <param name="defaultOption">Valeur par défaut</param>
        /// <returns></returns>
        SelectList GetSelectList(Expression<Func<TEntity, SelectListItem>> selector, Expression<Func<TEntity, bool>> predicate = null, object selectedValue = null, SelectListItem defaultOption = null);
        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns>Retourne la liste de Entité</returns>
        IList<TEntity> SqlQueryToList(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns>Retourne la liste de TModel</returns>
        IList<TModel> SqlQueryToList<TModel>(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns>Retourne un objet de type Entité</returns>
        TEntity SqlQueryToObject(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Execute SQL or Stored Procedure
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        /// <returns>Retourne un objet de type TModel</returns>
        TModel SqlQueryToObject<TModel>(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Execute SQL
        /// </summary>
        /// <param name="sqlOrProcedure"></param>
        /// <param name="args"></param>
        void ExecuteNoQuery(string sqlOrProcedure, params SqlParameter[] args);

        /// <summary>
        /// Delete all rows
        /// </summary>
        void DeleteAll();
    }
}