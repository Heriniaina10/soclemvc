﻿using System;

namespace Application.Etech.Core.Models
{
    public class DefaultLogWriter : ILogWriter
    {
        public void Info(string msg)
        {
            Console.WriteLine("INFO : {0}", msg);
        }

        public void Error(Exception e)
        {
            if(e==null) return;
            Error(e.Message);
            Error(e.InnerException);
        }

        public void Error(string msg)
        {
            Console.WriteLine("ERROR : {0}", msg);
        }
    }
}