﻿using System;
using System.Collections.Generic;

namespace Application.Etech.Core.Models
{
    public class EventLogController
    {
        #region FIELDS
        #endregion
        private static  readonly DefaultLogWriter _logger = new DefaultLogWriter();
        #region PROPERTIES
        #endregion

        #region CONSTRUCTOR
        #endregion

        #region METHODS

        public static bool AddNewEntry(string message, ImportBatchEventType type)
        {
            try
            {
                string currentPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                string fileName = currentPath + @"\Log\log.txt";
                if (!System.IO.Directory.Exists(currentPath + @"\Log"))
                {
                    System.IO.Directory.CreateDirectory(currentPath + @"\Log");
                }

                System.IO.File.AppendAllText(fileName,Environment.NewLine + DateTime.Now.ToString("dd/MM/yyy HH:mm:ss") + "  " + type.ToString() + ": " + message);
                switch (type)
                {
                    case ImportBatchEventType.Error:
                        _logger.Error(message);
                        break;
                    case ImportBatchEventType.Information:
                        _logger.Info(message);
                        break;
                    case ImportBatchEventType.Warning:
                        _logger.Info(message);
                        break;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }



        public static void HandleException(Exception ex)
        {
            try
            {
                List<Exception> exceptions = new List<Exception>();
                Exception exception = ex;
                while (exception != null)
                {
                    exceptions.Add(exception);
                    exception = exception.InnerException;
                }

                for (int level = 0; level < exceptions.Count; level++)
                {
                    exception = exceptions[level];
                    string exMsg = string.Format("#{0}/{1}({2}):{3}\r\n{4}", level + 1, exceptions.Count, exception.GetType().Name, exception.Message, exception.StackTrace);
                    //exception = exception.InnerException;
                    // to avoid: System.ArgumentException: Log entry string is too long. A string written to the event log cannot exceed 32766 characters.
                    //if (exMsg != null && exMsg.Length >= 32765)
                    //    exMsg = exMsg.Substring(0, 32764);
                    EventLogController.AddNewEntry(exMsg, ImportBatchEventType.Error);
                }
            }
            catch (Exception ex1)
            {
                EventLogController.AddNewEntry(ex1.ToString(), ImportBatchEventType.Error);
            }
        }

        #endregion

        #region EVENTS
        #endregion
    }

    #region ENUM

    /// <summary>
    /// Enum for the event id for ImportBatch
    /// </summary>
    public enum ImportBatchEventType : int
    {
        Information = 1000,
        Warning = 1001,
        Error = 1002
    }

    #endregion
}
