﻿namespace Application.Etech.Core.Models
{
    /// <summary>
    /// Label value pour combobox ou autre
    /// </summary>
    /// <typeparam name="TValue">Valeur</typeparam>
    /// <typeparam name="TLabel">Text ou label</typeparam>
    public class LabelValue<TValue, TLabel>
    {
        public TValue Value { get; set; }
        public TLabel Label { get; set; }
        public string Group { get; set; }
    }
}