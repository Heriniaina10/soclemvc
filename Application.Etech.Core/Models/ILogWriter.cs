﻿using System;

namespace Application.Etech.Core.Models
{
    public interface ILogWriter
    {
        void Info(string msg);

        void Error(Exception e);

        void Error(string msg);
    }
}