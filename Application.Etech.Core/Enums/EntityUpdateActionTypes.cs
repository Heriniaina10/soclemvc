﻿using System;

namespace Application.Etech.Core.Enums
{
    [Flags]
    public enum EntityUpdateActionTypes
    {
        Added = 1,
        BeforeUpdate = 2,
        AfterUpdate = 3,
        Deleted = 4,
    }
}
