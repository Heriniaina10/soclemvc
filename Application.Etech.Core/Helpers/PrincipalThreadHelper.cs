﻿namespace Application.Etech.Core.Helpers
{
    public static class PrincipalThreadHelper
    {
        public static string GetCurrentUserName()
        {
            return System.Threading.Thread.CurrentPrincipal.Identity.Name;
        }
    }
}
