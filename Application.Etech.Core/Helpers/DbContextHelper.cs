﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.MappingViews;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using InteractivePreGeneratedViews;
using Mehdime.Entity;

namespace Application.Etech.Core.Helpers
{
    public static class DbContextHelper
    {
        private static readonly Lazy<IDbContextScopeFactory> ScopeFactory = new Lazy<IDbContextScopeFactory>(() => new DbContextScopeFactory());

        private static readonly Lazy<IAmbientDbContextLocator> Locator = new Lazy<IAmbientDbContextLocator>(() => new AmbientDbContextLocator());

        public static bool CheckConnection<TDbContext>() where TDbContext : DbContext
        {
            try
            {
                using (ScopeFactory.Value.CreateReadOnly(DbContextScopeOption.ForceCreateNew))
                {
                    var context = Locator.Value.Get<TDbContext>();
                    context.Database.Connection.Open();
                    context.Database.Connection.Close();
                }
            }
            catch (SqlException)
            {
                return false;
            }
            return true;
        }

        public static bool CheckConnection<TDbContext>(string connectionString) where TDbContext : DbContext
        {
            try
            {
                using (ScopeFactory.Value.CreateReadOnly(DbContextScopeOption.ForceCreateNew))
                {
                    var context = Locator.Value.Get<TDbContext>();
                    context.Database.Connection.ConnectionString = connectionString;
                    context.Database.Connection.Open();
                    context.Database.Connection.Close();
                }
            }
            catch (SqlException)
            {
                return false;
            }
            return true;
        }

        public static bool InitDb<TSeedInitializer, TDbContext>(TDbContext ctx) where TDbContext : DbContext where TSeedInitializer: ISeedInitializer<TDbContext>
        {
            try
            {
                var seedInitializer = Activator.CreateInstance<TSeedInitializer>();
                seedInitializer.Seed(ctx); 
            }
            catch (SqlException)
            {
                return false;
            }
            return true;
        }
       
        public static bool GenerateEFViewCache<TDbContext, TEntity>(DbMappingViewCacheFactory viewCacheFactory) where TDbContext : DbContext where TEntity: class, new()
        {
            try
            {
                using (ScopeFactory.Value.CreateReadOnly(DbContextScopeOption.ForceCreateNew))
                {
                    var context = Locator.Value.Get<TDbContext>();
                    InteractiveViews.SetViewCacheFactory(context, viewCacheFactory);
                    context.Set<TEntity>().Count();
                }
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }
        
        public static bool GenerateEFCache<TDbContext, TEntity>() where TDbContext : DbContext where TEntity: class, new()
        {
            try
            {
                using (ScopeFactory.Value.CreateReadOnly(DbContextScopeOption.ForceCreateNew))
                {
                    var context = Locator.Value.Get<TDbContext>();
                    context.Set<TEntity>().Count();
                }
            }
            catch (SqlException)
            {
                return false;
            }
            return true;
        }
        
    }
}