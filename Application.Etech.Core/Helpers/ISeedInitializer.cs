﻿namespace Application.Etech.Core.Helpers
{
    public interface ISeedInitializer<TContent>
    {
        void Seed(TContent context);
    }
}