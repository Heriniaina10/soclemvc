﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SocleMvc.Web.Models
{
    public class FichierDto
    {
        public String Type { get; set; }
        public int IdDossier { get; set; }
        [Display(Name = "Mot de passe de protection du fichier")]
        [Required(ErrorMessage = "Ce champ est requis")]
        public String Password { get; set; }
        public String Path { get; set; }
    }
}