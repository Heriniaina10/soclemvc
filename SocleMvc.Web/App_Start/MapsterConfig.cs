﻿using Mapster;
using Socle.Constraints.Mappings;
//using FastExpressionCompiler;

namespace SocleMvc.Web
{
    public class MapsterConfig
    {
        public static void RegisterMapper()
        {
            //TypeAdapterConfig.GlobalSettings.Compiler = exp => exp.CompileFast();
            TypeAdapterConfig.GlobalSettings.Scan(typeof(PersonneMapping).Assembly);
        }
    }
}