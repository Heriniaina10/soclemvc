﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Application.Etech.Core.Errors;
using Application.Etech.Core.Security.Attributes;
using Newtonsoft.Json.Converters;

namespace SocleMvc.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuration et services de l'API Web
            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            //config.Filters.Add(new HostAuthenticationFilter(DefaultAuthenticationTypes.ExternalBearer));
            //config.Filters.Add(new ApplicationExceptionFilter());
            //config.Filters.Add(new ClaimsAuthorizeAttribute());

            //config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            //config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new IsoDateTimeConverter()
            //{
            //    DateTimeFormat = "dd/MM/yyyy"
            //});


            ////config.MessageHandlers.Insert(0, new ServerCompressionHandler(new GZipCompressor(), new DeflateCompressor()));

            //config.Services.Replace(typeof(IExceptionLogger), new Log4netExceptionLogger());
            //config.Services.Replace(typeof(IExceptionHandler), new UnmanagedExceptionHandler());


            //CORS
            //            var cors = new EnableCorsAttribute("*", "*", "*");
            //            config.EnableCors(cors);

            // Itinéraires de l'API Web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.MessageHandlers.Insert(0, new ServerCompressionHandler(0, new GZipCompressor(RecyclableStreamManager.Instance), new DeflateCompressor(RecyclableStreamManager.Instance)));
        }
    }
}
