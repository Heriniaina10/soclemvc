﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Application.Etech.Core.MVC;
using Mehdime.Entity;
using Socle.Applicatif.Person;
using Socle.Data.Context;
using Socle.Service.Person;

namespace SocleMvc.Web.Controllers
{
    public class PersonsController : BaseController
    {
        // GET: Persons
        public ActionResult Index()
        {
            var person = PersonApplicatif.Instance.Find(1);

            PersonService.Instance.Andrana();

            return View();
        }
    }
}