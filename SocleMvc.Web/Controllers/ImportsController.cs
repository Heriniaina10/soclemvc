﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Application.Etech.Core.Models;
using Socle.Data.Constantes;
using Application.Etech.Core.MVC;
using FileHelpers;
using Mapster;
using Socle.Applicatif.Personne;
using Socle.Applicatif.Region;
using Socle.Data.Dto.Personne;
using Socle.Data.Dto.Result;
using Socle.Data.Entities;
using Socle.Data.Enums;
using Socle.Service.Commune;
using Socle.Service.District;
using Socle.Service.Fokontany;
using Socle.Service.Fonction;
using Socle.Service.Province;
using Socle.Service.Region;

namespace SocleMvc.Web.Controllers
{
    public class ImportsController : BaseController
    {
        // GET: Imports
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadOfFile(HttpPostedFileBase fileUpload)
        {
            if (ModelState.IsValid)
            {
                if (fileUpload != null)
                {
                    if (fileUpload.FileName.EndsWith(".csv"))
                    {
                        try
                        {
                            var path = Server.MapPath(UtilityConstant.ImportOutputPath);
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            var files = $"{DateTime.Now.ToString("yyyyMMddHHmmss")}-{Path.GetFileName(fileUpload.FileName)}";
                            //Directory.CreateDirectory(path);

                            Logger.Info($"Sauvegarde du fichier PRODUIT [{files}] dans [{path}]");
                            // file is uploaded
                            path = Path.Combine(path, files);
                            fileUpload.SaveAs(path);
                            Logger.Info($"Sauvegarde du fichier PRODUIT [{files}] => OK");

                            Logger.Info(">> Début Import PRODUIT ...");
                            var result = ImportCsv(path);
                            Logger.Info(">> Fin Import PRODUIT");

                            return View("RapportImport", result);
                        }
                        catch (Exception e)
                        {
                            Logger.Error("Erreur rencontré pendant l'import PRODUIT : ", e);
                        }
                    }
                    //return RedirectToAction("MessageTypeFichier", "Error");
                }
                //return RedirectToAction("MessageImportVide", "Error");
            }
            return RedirectToAction("Index", "Errors");
        }

        public ResultImportDto ImportCsv(string filename)
        {
            /*
             * - Lire fichier
             * - Si erreur format date ou autre, ajout dans liste erreur format
             * - Si doublon, ajout dans liste doublon OF
             * - Sinon, on insere dans la base
             * 
             * */
            var result = new ResultImportDto();
            var codes = new List<string>();
            try
            {
                using (var scope = ContextScopeFactory.CreateWithTransaction(IsolationLevel.ReadUncommitted))
                {
                    var engine = new DelimitedFileEngine<PersonneCsvDto>();
                    engine.Options.IgnoreFirstLines = 9;
                    // Enregistrer les erreurs et continue à traiter la prochaine ligne
                    engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
                    //Traitement doublon
                    engine.AfterReadRecord += (eng, args) =>
                    {
                        var record = args.Record;
                        //Duplication au niveau Fichier et non pas dans Bdd, si dans BDD, on update l'enregistrement
                        if (codes.Any(x => x == $"{record.NomComplet}_{record.CIN}"))
                        {
                            Logger.Info($"Doublon CADENCE : NUM[{args.LineNumber}] -> LIGNES[{record.NomComplet}] -> CODE PRODUIT[{record.CIN}]");
                            //AddRecordTDuplicateList(result, record, args.LineNumber);
                            args.SkipThisRecord = true;
                        }
                        codes.Add($"{record.NomComplet}_{record.CIN}");
                    };
                    var datas = engine.ReadFileAsList(filename);
                    //total row
                    result.TotalRow = engine.TotalRecords;

                    //Traitement OF invalide
                    if (engine.ErrorManager.HasErrors)
                    {
                        Logger.Info($"Traitement de(s) {engine.ErrorManager.ErrorCount} ligne(s) érronée(s)");
                        AddRecordToErrorList(result, engine.ErrorManager.Errors);
                    }

                    //Traitement OF valide
                    if (datas != null && datas.Count > 0)
                    {
                        Logger.Info($"Début traitement de(s) {datas.Count} ligne(s) valide");

                        // var personnes = datas.Adapt<IList<Personne>>()?.ToArray();
                        // PersonneApplicatif.Instance.Add(personnes);
                        // scope.SaveChanges();
                        
                        
                        // TODO :: rh tsis ny district fa commune sy region ar province mis d las ts ajouté le info amzw

                        var provinces = ProvinceService.Instance.FindAll() ?? new List<Province>(); // use dapper
                        var regions = RegionService.Instance.FindAll() ?? new List<Region>();
                        var districts = DistrictService.Instance.FindAll() ?? new List<District>();
                        var communes = CommuneService.Instance.FindAll() ?? new List<Commune>();
                        var fokontanies = FokontanyService.Instance.FindAll() ?? new List<Fokontany>();
                        var fonctions = FonctionService.Instance.FindAll() ?? new List<Fonction>();

                        foreach (var personCsv in datas)
                        {
                            try
                            {
                                #region Province

                                var province = provinces?.FirstOrDefault(
                                    p => !string.IsNullOrEmpty(personCsv?.Province)
                                         && p.Nom.Contains(personCsv.Province));
                                if (province != null)
                                {
                                    // TODO
                                }
                                else
                                {
                                    province = new Province
                                    {
                                        Nom = personCsv.Province
                                    };

                                    ProvinceService.Instance.Add(province);
                                    provinces.Add(province);
                                }

                                #endregion

                                #region Region

                                var region = regions?.FirstOrDefault(
                                    p => !string.IsNullOrEmpty(personCsv?.Region)
                                         && p.Nom.Contains(personCsv.Region));
                                if (region != null)
                                {
                                    // TODO
                                }
                                else
                                {
                                    region = new Region
                                    {
                                        Nom = personCsv.Region,
                                        Province = province
                                    };

                                    RegionService.Instance.Add(region);
                                    regions.Add(region);
                                }

                                #endregion

                                #region Commune

                                var commune = communes?.FirstOrDefault(
                                    p => !string.IsNullOrEmpty(personCsv?.Commune)
                                         && p.Nom.Contains(personCsv.Commune));
                                if (commune != null)
                                {
                                    // TODO
                                }
                                else
                                {
                                    commune = new Commune
                                    {
                                        Nom = personCsv.Commune,
                                        Region = region
                                    };

                                    CommuneService.Instance.Add(commune);
                                    communes.Add(commune);
                                }

                                #endregion

                                #region District

                                var district = districts?.FirstOrDefault(
                                    p => !string.IsNullOrEmpty(personCsv?.District)
                                         && p.Nom.Contains(personCsv.District));
                                if (district != null)
                                {
                                    // TODO
                                }
                                else
                                {
                                    district = new District
                                    {
                                        Nom = personCsv.District,
                                        Commune = commune
                                    };

                                    DistrictService.Instance.Add(district);
                                    districts.Add(district);
                                }

                                #endregion

                                #region Fokontany

                                var fkt = fokontanies?.FirstOrDefault(
                                    p => !string.IsNullOrEmpty(personCsv?.Eglise)
                                         && p.Nom.Contains(personCsv.Eglise));
                                if (fkt != null)
                                {
                                    // TODO
                                }
                                else
                                {
                                    fkt = new Fokontany
                                    {
                                        Nom = personCsv.Eglise,
                                        District = district
                                    };

                                    FokontanyService.Instance.Add(fkt);
                                    fokontanies.Add(fkt);
                                }

                                #endregion

                                var personne = personCsv.Adapt<Personne>();
                                personne.Fokontany = fkt;
                                var fonction = fonctions.FirstOrDefault(f => f.Nom.Contains("mpino"));
                                personne.Fonction = fonction;

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }
                        }

                        scope.SaveChanges();

                        //var produit  = ProduitRepository.FirstOrDefault(p => p.Ligne == x.Ligne && p.Code == x.Article)
                        //var prodToAdds = new List<Produit>();
                        //var prodToUpdates = new List<Produit>();
                        //var ofToUpdate = new List<OrdreFabrication>();
                        //ordres.ForEach(x =>
                        //{
                        //    var of = OrdreFabricationRepository.Instance.FirstOrDefault(o => (o.CodeArticle ?? "").Equals(x.CodeProduit, StringComparison.InvariantCultureIgnoreCase) &&
                        //                        (o.Ligne ?? "").Equals(x.Lignes, StringComparison.InvariantCultureIgnoreCase));
                        //    if (of != null && of.Cadence == null)
                        //    {
                        //        of.Cadence = x.Cadence;
                        //        ofToUpdate.Add(of);
                        //    }
                        //    var prod = ProduitRepository.FirstOrDefault(p => (p.Code ?? "").Equals(x.CodeProduit, StringComparison.InvariantCultureIgnoreCase) && (p.Ligne ?? "").Equals(x.Lignes, StringComparison.InvariantCultureIgnoreCase));
                        //    if (prod == null)
                        //    {
                        //        prodToAdds.Add(new Produit
                        //        {
                        //            Code = x.CodeProduit,
                        //            Ligne = x.Lignes,
                        //            Nom = x.Produits,
                        //            Cadence = x.Cadence ?? 0
                        //        });
                        //    }
                        //    else
                        //    {
                        //        prod.Cadence = x.Cadence ?? 0;
                        //        prodToUpdates.Add(prod);
                        //    }
                        //});
                        //if (prodToAdds.Any()) ProduitRepository.Add(prodToAdds.ToArray());
                        //if (prodToUpdates.Any()) ProduitRepository.Update(prodToUpdates.ToArray());
                        //if (ofToUpdate.Any()) OrdreFabricationRepository.Instance.Update(ofToUpdate.ToArray());
                        //var res = scope.SaveChanges();
                        //result.Operation = res > 0 ? OperationFlags.SUCCESS : OperationFlags.ERROR;
                        //if (result.Operation == OperationFlags.SUCCESS)
                        //{
                        //    result.ValidRow = prodToAdds.Count + prodToUpdates.Count;
                        //}
                        Logger.Info($"Fin traitement de(s) {datas.Count} ligne(s) valide avec FLAG[{result.Operation}]");
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                result.Operation = OperationFlags.ERROR;
                Logger.Error($"Impossible de trouver le fichier [{filename}]", e);
            }
            catch (Exception e)
            {
                result.Operation = OperationFlags.ERROR;
                Logger.Error($"Erreur import : fichier[{filename}]", e);
            }

            return result;
        }

        private static void AddRecordToErrorList(ResultImportDto result, ErrorInfo[] errors)
        {
            if (result == null || errors == null) return;
            foreach (var error in errors)
            {
                var data = error.RecordString?.Split(';');
                if (data == null)
                {
                    result.ResultErreurFormats.Add(new ResultTemporaire
                    {
                        NumeroLigne = error.LineNumber,
                        MessageErreur = "Ligne vide"
                    });
                }
                else
                {
                    var taille = data.Length;
                    var message = error.ExceptionInfo.Message;
                    int idx = message.IndexOf("==>");
                    if (idx > 0) message = message.Substring(idx + 3);
                    else if (error.ExceptionInfo.Message.Contains("The value is empty and must be populated."))
                    {
                        var field = ((ConvertException)error.ExceptionInfo).FieldName;
                        message = string.Format("Le champ '{0}' ne peut pas être vide.", field);
                    }
                    else message = "Erreur template. Le nombre de colonnes doit être égale à 4.";
                    //result.ResultErreurFormats.Add(new ResultTemporaire
                    //{
                    //    CodeResult = taille > 1 ? data[1] : "",
                    //    Result = taille > 2 ? data[2] : "",
                    //    Cadence = taille > 3 ? data[3] : "",
                    //    NumeroLigne = error.LineNumber,
                    //    MessageErreur = message,
                    //    Ligne = taille > 0 ? data[0] : ""
                    //});
                }
            }
        }



        [HttpPost]
        public ActionResult UploadFileOld(HttpPostedFileBase fileUpload)
        {
            var result = 0;

            if (fileUpload == null)
            {
                return null;
            }

            var path = Server.MapPath(UtilityConstant.ImportOutputPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var filePath = path + Path.GetFileName(fileUpload.FileName);
            var extension = Path.GetExtension(fileUpload.FileName);
            fileUpload.SaveAs(filePath);

            var conString = string.Empty;
            switch (extension)
            {
                case UtilityConstant.ExtensionXls: //Excel 97-03.
                    conString = WebConfigurationManager.AppSettings[UtilityConstant.Xls];
                    break;
                case UtilityConstant.ExtensionXlsx: //Excel 07 and above.
                    conString = WebConfigurationManager.AppSettings[UtilityConstant.Xlsx];
                    break;
            }

            var dt = new DataTable();
            conString = string.Format(conString, filePath);

            using (var connExcel = new OleDbConnection(conString))
            {
                using (var cmdExcel = new OleDbCommand())
                {
                    using (var odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        var dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        int i = 0;
                        while (i < dtExcelSchema?.Rows.Count)
                        {
                            var sheetName = dtExcelSchema.Rows[i][UtilityConstant.TableName].ToString();
                            connExcel.Close();

                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";


                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(dt);
                            connExcel.Close();
                            i++;
                        }
                    }
                }
            }

            return null;
        }

        //private void AfterReadRecord(object eng, EventArgs args)
        //{
        //    var record = args.Record;
        //    //Duplication au niveau Fichier et non pas dans Bdd, si dans BDD, on update l'enregistrement
        //    if (codes.Any(x => x == $"{record.NomComplet}_{record.CIN}"))
        //    {
        //        Logger.Info($"Doublon CADENCE : NUM[{args.LineNumber}] -> LIGNES[{record.NomComplet}] -> CODE PRODUIT[{record.CIN}]");
        //        //AddRecordTDuplicateList(result, record, args.LineNumber);
        //        args.SkipThisRecord = true;
        //    }
        //    codes.Add($"{record.NomComplet}_{record.CIN}");
        //}
    }
}