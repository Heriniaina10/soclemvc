﻿using System.Web.Mvc;
using Application.Etech.Core.MVC;

namespace SocleMvc.Web.Controllers
{
    public class ErrorsController : BaseController
    {
        // GET: Error
        /// <summary>
        /// page index error
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.MessageErreur = "Erreur import";
            return View();
        }

        /// <summary>
        /// message a afficher si aucun fichier à importer n'est choisi
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageImportVide()
        {
            ViewBag.MessageErreur = "Veuillez choisir un fichier à importer ";
            return View();
        }

        /// <summary>
        /// message a afficher si le fichier saisie pour l'import n'est pas csv
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageTypeFichier()
        {
            ViewBag.MessageErreur = "Veuillez choisir un fichier csv pour l'import ";
            return View();
        }

        /// <summary>
        /// message à afficher si le fichier choisie n'a pas les chmp complet ou chmp dépassé 
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageModeleDeDonnees()
        {
            ViewBag.MessageErreur = "Modèle de données non correspondant pour ce fichier";
            return View();
        }

        /// <summary>
        /// message à affichier si le fichier contient un numero of avec doublon
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageDoublonDansFichier()
        {
            ViewBag.MessageErreur = "Doublon dans le fichier";
            return View();
        }
    }
}