﻿using System.Net;
using Application.Etech.Core.Errors.Exceptions;
using Socle.Applicatif.Person;
using Socle.Data.Dto;
using Socle.Data.Entities;
using System.Web.Http;
using System.Web.Http.Description;
using Socle.Data.Dto.Personne;

namespace SocleMvc.Web.Controllers.Api
{
    //[RoutePrefix("api/persons")]
    [Route("api/persons")] // pour empêcher que le get utilisant ?id=xxx marche pour que l'API soit un RESTFul
    public class PersonsController : ApiController
    {
        [ResponseType(typeof(Person))]
        public IHttpActionResult GetAll()
        {
            var persons = PersonApplicatif.Instance.FindAll();

            return Ok(persons);
        }

        [ResponseType(typeof(bool))]
        [Route("api/persons/{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var person = PersonApplicatif.Instance.Find(id);

            return Ok(person);
        }

        //[HttpPost]
        [ResponseType(typeof(Person))]
        public IHttpActionResult Post([FromBody] PersonDto person)
        {
            if(!ModelState.IsValid)
            {
                throw new FunctionalException("Person not valid", ModelState);
            }
            PersonApplicatif.Instance.Add(person);

            return Ok(person);
        }

        [ResponseType(typeof(Person))]
        public IHttpActionResult Put(int id, [FromBody] PersonDto person)
        {
            if (person == null)
            {
                throw new FunctionalException("Person not exist", null, HttpStatusCode.NotFound);
            }
            if (!ModelState.IsValid)
            {
                throw new FunctionalException("Person not valid", ModelState);
            }
            if (person.Id != id)
            {
                throw new FunctionalException("Person and Ressource Id not conform");
            }

            PersonApplicatif.Instance.Update(person);

            return Ok(person);
        }

        [ResponseType(typeof(bool))]
        [Route("api/persons/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var person = PersonApplicatif.Instance.Find(id);
            if (person == null)
            {
                //return NotFound();
                throw new FunctionalException("Person not exist", ModelState);
            }
            PersonApplicatif.Instance.DeleteById(id);

            return Ok(true);
        }
    }
}
