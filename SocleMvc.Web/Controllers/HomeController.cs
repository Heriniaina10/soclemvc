﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Application.Etech.Core.MVC;
using SocleMvc.Web.Models;

namespace SocleMvc.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult ExportXls()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ExportXls(FichierDto model, bool isToPdf = false, string returnUrl = null)
        {
            try
            {

                using (var scope = ContextScopeFactory.Create())
                {
                    //var dossier = DossierService.Instance.FirstOrDefault(x => x.IdDossier == model.IdDossier);
                    var nomDossier = "Test"; // DossierService.Instance.GetNom(dossier.IdDemande);



                    if (!isToPdf)
                    {
                        model.Path = @"C:\";
                    }

                    else
                    {
                        Directory.CreateDirectory(model.Path);
                    }

                    if (!Directory.Exists(model.Path))
                    {
                        Response.Write("<script>alert(\"Ce chemin est invalide\");</script>");
                        //return PartialView("_ExportXlsFichier", model);

                    }

                    var serverPath = System.Web.HttpContext.Current.Server.MapPath(
                        "~/App_Data/Dossiers/" + nomDossier);

                    MemoryStream memoryStream = new MemoryStream();

                    string fileName = nomDossier + ".xls";



                    //switch (model.Type)
                    //{
                    //    case "Sommaire":
                    //        memoryStream =
                    //            SommaireService.Instance.SommaireToCsv("Sommaire", serverPath, dossier, model.Password);

                    //        break;
                    //    case "Bilan actuel":
                    //        memoryStream = new MemoryStream();
                    //        break;
                    //    case "Bilan après décaissement":
                    //        memoryStream = new MemoryStream();
                    //        break;
                    //    case "CashFlow":
                    //        memoryStream = new MemoryStream();
                    //        break;
                    //    case "Autres CashFlow":
                    //        memoryStream = new MemoryStream();
                    //        break;
                    //    case "Garanties":
                    //        memoryStream = GarantieService.Instance.GarantieToCsv("Garanties", model.Path, dossier);
                    //        break;
                    //    case "Décision du comité avec les conditions à prescrire":
                    //        memoryStream = new MemoryStream();
                    //        break;
                    //    default:
                    //        memoryStream = new MemoryStream();
                    //        break;
                    //}

                    File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        fileName);


                    FileInfo file = new FileInfo(Path.Combine(serverPath, fileName));
                    string filePath = Path.Combine(serverPath, fileName);

                    byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);
                    // string fileName = "myfile.ext";

                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                    FileInfo fichier = new FileInfo(filePath);
                    if (fichier.Exists)
                    {
                        // Clear Rsponse reference  
                        Response.Clear();
                        // Specify content type  
                        Response.ContentType = "application/vnd.ms-excel";
                        // Add header by specifying file name  
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + fichier.Name);
                        // Add header for content length  
                        Response.AddHeader("Content-Length", fichier.Length.ToString());
                        // Specify content type  

                        // Clearing flush  
                        Response.Flush();
                        // Transimiting file  
                        Response.TransmitFile(fichier.FullName);
                        Response.End();


                    }
                    return RedirectToAction("Index");
                }
            }
            catch (Exception exception)
            {
                throw exception;
                return null;
            }
        }

        public FileResult Download()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"c:\folder\myfile.ext");
            string fileName = "myfile.ext";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}