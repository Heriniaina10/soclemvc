﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Mapster;
using Socle.Data.Dto.Personne;
using Socle.Data.Entities;

namespace SocleMvc.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MapsterConfig.RegisterMapper();
        }

        /// <summary>
        /// Lors qu'une erreur se produite
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                ex = ex.GetBaseException();
                Logger.Info("*********** Erreur trouvée :  " + DateTime.Now.ToShortTimeString() + " ===> " + ex.GetBaseException() + " *************");
                //Logger.Info("***********Erreur lié au réseau et/ou serveur :  " + DateTime.Now.ToShortTimeString() + "*************");
            }
            Server.ClearError();
        }
    }
}
